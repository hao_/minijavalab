build:
	javac -d bin -sourcepath src src/minijavac/MiniJavaC.java

typecheck_test: build
	scripts/typecheck_test.sh

piglet_test: build
	scripts/piglet_test.sh

spiglet_test: build
	scripts/spiglet_test.sh

kanga_test: build
	scripts/kanga_test.sh

mips_test: build
	scripts/mips_test.sh

mips_runall: build
	scripts/mips_runall.sh

minijava_test: build
	scripts/minijava_test.sh

clean:
	rm -rf bin out *.out *.s

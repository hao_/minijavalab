
class Polymorphic {
	public static void main(String[] args) {
		A a ;
		A c ;
		B b ;
		int k;

		a = new A();
		b = new B();
		c = b;

		k = a.assignA(1);
		k = b.assignA(3);
		k = b.assignB(2);

		k = a.print(); // 1
		k = b.print(); // 2
		k = c.print(); // 2

		k = a.printA(); // 1
		k = b.printA(); // 3
		k = c.printA(); // 3
	}
}

class A {
	int type ;
	public int assignA(int type1) {
		type = type1;
		return type;
	}
	public int printA() {
		System.out.println(type);
		return type;
	}
	public int print() {
		System.out.println(type);
		return type;
	}
}

class B extends A {
	int type ;
	public int assignB(int type1) {
		type = type1;
		return type;
	}
	public int printB() {
		System.out.println(type);
		return type;
	}
	public int print() {
		System.out.println(type);
		return type;
	}
}
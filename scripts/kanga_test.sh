for file in samples/kanga/*.spg 
do
	if test -f $file
	then
		na=${file:0:${#file}-4}
		echo "CASE: "$na
		java -cp bin minijavac.MiniJavaC spiglet2kanga $na.spg a.out
		java -jar jar/kgi.jar < a.out > ans.out
		java -jar jar/kgi.jar < $na.kg > ans.std
		diff ans.out ans.std
		if [ $? -eq 0 ] 
		then
			echo "PASS"
		fi
		rm a.out ans.out ans.std
		echo ""
	fi
done

for file in samples/mips/*.kg 
do
	if test -f $file
	then
		na=${file:0:${#file}-3}
		echo "Tranlating: "$na
		java -cp bin minijavac.MiniJavaC kanga2mips $na.kg ${na##*/}_out.s
		echo ""
	fi
done

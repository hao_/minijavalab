for file in samples/mips/*.kg 
do
	if test -f $file
	then
		na=${file:0:${#file}-3}
		echo "CASE: "$na
		java -cp bin minijavac.MiniJavaC kanga2mips $na.kg a.out
		spim -file a.out|awk 'NR>1'|cat > ans.out
		spim -file $na.s|awk 'NR>1'|cat > ans.std
		diff ans.out ans.std
		if [ $? -eq 0 ] 
		then
			echo "PASS"
		fi
		rm a.out ans.out ans.std
		echo ""
	fi
done

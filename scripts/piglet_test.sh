for file in samples/piglet/*.pg 
do
	if test -f $file
	then
		na=${file:0:${#file}-3}
		echo "CASE: "$na
		java -cp bin minijavac.MiniJavaC piglet $na.java a.out
		java -jar jar/pgi.jar < a.out > ans.out
		java -jar jar/pgi.jar < $na.pg > ans.std
		diff ans.out ans.std
		if [ $? -eq 0 ] 
		then
			echo "PASS"
		fi
		rm a.out ans.out ans.std
		echo ""
	fi
done

for file in samples/spiglet/*.pg 
do
	if test -f $file
	then
		na=${file:0:${#file}-3}
		echo "CASE: "$na
		java -cp bin minijavac.MiniJavaC piglet2spiglet $na.pg a.out
		java -jar jar/spp.jar < a.out
		if [ $? -ne 0 ]
		then
			echo "Failed to parse spiglet output"
			continue
		fi
		java -jar jar/pgi.jar < a.out > ans.out
		java -jar jar/pgi.jar < $na.spg > ans.std
		diff ans.out ans.std
		if [ $? -eq 0 ] 
		then
			echo "PASS"
		fi
		rm a.out ans.out ans.std
		echo ""
	fi
done

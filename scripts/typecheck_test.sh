for file in samples/typecheck/* 
do
	if test -f $file
	then
		echo "CASE: "$file	
		java -cp bin minijavac.MiniJavaC typecheck $file
		echo ""
	fi
done

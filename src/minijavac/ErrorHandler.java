package minijavac;

public class ErrorHandler {
    public static void TypeCheckError(String info) {
        System.err.println("Typecheck Failed.");
        System.err.println(info);
        System.exit(1);
    }

    public static void UnknownError(String info) {
        System.err.printf("UnknownError: %s\n", info);
        for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
            System.err.println(ste);
        }
        System.exit(1);
    }

    public static void PigletError(String info) {
        System.err.printf("PigletError: %s\n", info);
        for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
            System.err.println(ste);
        }
        System.exit(1);
    }

    public static void SpigletError(String info) {
        System.err.printf("SpigletError: %s\n", info);
        for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
            System.err.println(ste);
        }
        System.exit(1);
    }

    public static void KangaError(String info) {
        System.err.printf("KangaError: %s\n", info);
        for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
            System.err.println(ste);
        }
        System.exit(1);
    }

    public static void MIPSError(String info) {
        System.err.printf("MIPSError: %s\n", info);
        for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
            System.err.println(ste);
        }
        System.exit(1);
    }

}

package minijavac;

import java.io.*;

import minijavac.kanga.KangaProgram;
import minijavac.kanga.parser.KangaParser;
import minijavac.minijava.parser.MiniJavaParser;
import minijavac.minijava.parser.ParseException;

import minijavac.minijava.symbols.MSymbolTable;
import minijavac.minijava.syntaxtree.Node;
import minijavac.mips.MIPSProgram;
import minijavac.piglet.PigletProgram;
import minijavac.piglet.parser.PigletParser;
import minijavac.spiglet.SpigletProgram;
import minijavac.spiglet.parser.SpigletParser;

public class MiniJavaC {
    private static void printUsage() {
        System.out.println("Usage: MiniJavaC [COMMAND] [SOURCE FILE]");
        System.out.println("    or MiniJavaC [COMMAND] [SOURCE FILE] [DEST FILE]");
        System.out.println("");
        System.out.println("     COMMAND: typecheck          Only do type check");
        System.out.println("              piglet             Compile to piglet");
        System.out.println("              piglet2spiglet     Convert piglet program to spiglet");
        System.out.println("              spiglet2kanga      Convert spiglet program to kanga");
        System.out.println("              kanga2mips         Convert kanga program to mips");
        System.out.println("              minijava2mips      Convert MiniJava program to mips");
    }
    private static final String CommandTypeCheck = "typecheck";
    private static final String CommandPiglet = "piglet";
    private static final String CommandSpiglet = "piglet2spiglet";
    private static final String CommandKanga = "spiglet2kanga";
    private static final String CommandMIPS = "kanga2mips";
    private static final String CommandAll = "minijava2mips";

    private static void minijava2piglet(InputStream input, PrintStream output, boolean onlyTypecheck) {
        MiniJavaParser parser = new MiniJavaParser(input);
        MSymbolTable symbolTable = new MSymbolTable();
        try {
            Node root = parser.Goal();
            TypeCheck.check(root, symbolTable);

            if (onlyTypecheck) {
                return;
            }

            PigletProgram pigletProgram = new PigletProgram();
            pigletProgram.FromMiniJava(root, symbolTable);
            pigletProgram.print(output);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
    private static void piglet2spiglet(InputStream input, PrintStream output) {
        PigletParser parser = new PigletParser(input);
        SpigletProgram spigletProgram = new SpigletProgram();
        spigletProgram.FromPiglet(parser);
        spigletProgram.Print(output);
    }
    private static void spiglet2kanga(InputStream input, PrintStream output) {
        SpigletParser parser = new SpigletParser(input);
        KangaProgram kangaProgram = new KangaProgram();
        kangaProgram.FromSpiglet(parser);
        kangaProgram.Print(output);
    }
    private static void kanga2mips(InputStream input, PrintStream output) {
        KangaParser parser = new KangaParser(input);
        MIPSProgram mipsProgram = new MIPSProgram();
        mipsProgram.FromKanga(parser);
        mipsProgram.Print(output);
    }


    private static void compileMain(String command, String srcFile, String dstFile) {
        if (!(command.equals(CommandTypeCheck) || command.equals(CommandPiglet)
                || command.equals(CommandSpiglet) || command.equals(CommandKanga)
                || command.equals(CommandMIPS) || command.equals(CommandAll) )) {
            System.out.printf("Invalid command \"%s\"\n", command);
            printUsage();
            return;
        }

        try {
            // input
            File src = new File(srcFile);
            InputStream fin = new java.io.FileInputStream(src);

            // output
            PrintStream dstStream;
            if (dstFile != null) {
                dstStream = new PrintStream(dstFile);
            } else {
                dstStream = System.out;
            }

            // Command typecheck
            if (command.equals(CommandTypeCheck)) {
                minijava2piglet(fin, dstStream, true);
                return;
            }

            // Command piglet
            if (command.equals(CommandPiglet)) {
                minijava2piglet(fin, dstStream, false);
                return;
            }

            // Command piglet2spiglet
            if (command.equals(CommandSpiglet)) {
                piglet2spiglet(fin, dstStream);
                return;
            }

            // Command spiglet2kanga
            if (command.equals(CommandKanga)) {
                spiglet2kanga(fin, dstStream);
                return;
            }

            // Command kanga2mips
            if (command.equals(CommandMIPS)) {
                kanga2mips(fin, dstStream);
                return;
            }

            // Command minijava2mips
            ByteArrayOutputStream pigletStream = new ByteArrayOutputStream();
            ByteArrayOutputStream spigletStream = new ByteArrayOutputStream();
            ByteArrayOutputStream kangaStream = new ByteArrayOutputStream();
            minijava2piglet(fin, new PrintStream(pigletStream), false);
            piglet2spiglet(new ByteArrayInputStream(pigletStream.toByteArray()), new PrintStream(spigletStream));
            spiglet2kanga(new ByteArrayInputStream(spigletStream.toByteArray()), new PrintStream(kangaStream));
            kanga2mips(new ByteArrayInputStream(kangaStream.toByteArray()), dstStream);
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        }
    }

    public static void main(String args[]) {
        if (args.length == 2) {
            compileMain(args[0], args[1], null);
        } else if (args.length == 3) {
            compileMain(args[0], args[1], args[2]);
        } else {
            printUsage();
        }
    }
}

package minijavac;

public class Specification {
    public final static int MaxMethodParametersNum = 19;
    public final static String PigletIndent = "       ";
    public final static int PigletFirstTempUnit = 20;
    public final static int AddrSize = 4;
    public static String PigletFuncName(String className, String methodName) {
        return String.format("%s___%s", className, methodName);
    }
    public final static String PigletLabeledIndent(String label) {
        return String.format("%1$-7s", label);
    }
    public final static String MIPSLabeledIndent(String label) {
        return String.format("%1$-7s", label + ":");
    }

    public final static String MIPSMainFuncName = "main";
}

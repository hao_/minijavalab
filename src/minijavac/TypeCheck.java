package minijavac;

import minijavac.minijava.symbols.MSymbolTable;
import minijavac.minijava.syntaxtree.*;

import minijavac.minijava.JTBvisitor.*;
import minijavac.minijava.visitors.BodyCheckVisitor;
import minijavac.minijava.visitors.BuildSymbolTableVisitor;
import minijavac.minijava.visitors.ClassDeclareVisitor;

public class TypeCheck {
    public static boolean check(Node root, MSymbolTable symbolTable) {
        root.accept(new ClassDeclareVisitor(), symbolTable);
        root.accept(new BuildSymbolTableVisitor(), symbolTable);
        root.accept(new BodyCheckVisitor(), symbolTable);

        return true;
    }
}

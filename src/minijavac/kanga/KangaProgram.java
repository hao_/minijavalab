package minijavac.kanga;

import minijavac.spiglet.parser.Node;
import minijavac.spiglet.parser.ParseException;
import minijavac.spiglet.parser.SpigletParser;
import minijavac.spiglet.visitors.ToKangaVisitor;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Vector;

public class KangaProgram {
    private HashMap<String, KgProcedure> procMap;
    private Vector<KgProcedure> procList;

    public KangaProgram() {
        procMap = new HashMap<>();
        procList = new Vector<>();
    }

    public void FromSpiglet(SpigletParser parser) {
        try {
            Node goal = parser.Goal();
            ToKangaVisitor.VisitorEntry(goal, this);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public boolean addProcedure(KgProcedure proc) {
        if (procMap.containsKey(proc.getName())) {
            return false;
        } else {
            procMap.put(proc.getName(), proc);
            procList.addElement(proc);
            return true;
        }
    }

    public void Print(PrintStream out) {
        for (KgProcedure proc: procList) {
            proc.print(out);
        }
    }
}

package minijavac.kanga;

import java.util.HashSet;
import java.util.Set;

public class KangaSpecification {
    static final public String[] calleeSaver = {"s0", "s1", "s2", "s3", "s4", "s5", "s6", "s7"};
    static final public String[] callerSaver = {"t0", "t1", "t2", "t3", "t4", "t5", "t6", "t7", "t8", "t9"};
    static public String[] allTmpReg;
    static final public String returnReg = "v0";
    static final public String tmpReg = "v1";
    static final public String tmpResReg = "a3";
    static final public String[] arguReg = {"a0", "a1", "a2", "a3"};

    static Set<String> isCalleeSaverMap;
    static Set<String> isCallerSaverMap;
    static {
        isCalleeSaverMap = new HashSet<>();
        isCallerSaverMap = new HashSet<>();
        allTmpReg = new String[calleeSaver.length + callerSaver.length];
        int n = 0;
        // Caller save first
        for (String s: callerSaver) {
            isCallerSaverMap.add(s);
            allTmpReg[n++] = s;
        }
        for (String s: calleeSaver) {
            isCalleeSaverMap.add(s);
            allTmpReg[n++] = s;
        }
    }

    public static boolean isCalleeSave(String reg) {
        return isCalleeSaverMap.contains(reg);
    }
    public boolean isCallerSave(String reg) {
        return isCallerSaverMap.contains(reg);
    }
}

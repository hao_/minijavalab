package minijavac.kanga;

import minijavac.Specification;

import java.io.PrintStream;
import java.util.Vector;

public class KgProcedure {
    private String name;
    private Vector<String> initStmts;
    private Vector<String> stmts;
    public int argnum;
    public int spillnum;
    public int maxcallarg;
    private String nowlabel;

    public KgProcedure(String _name) {
        name = _name;
        stmts = new Vector<>();
        initStmts = new Vector<>();
        nowlabel = null;
    }

    public String getName() {
        return name;
    }

    private void addStmtWithLabel(String stmt, String label) {
        stmts.addElement(String.format("%s%s", Specification.PigletLabeledIndent(label), stmt));
    }

    public void addStmt(String stmt) {
        if (nowlabel != null) {
            addStmtWithLabel(stmt, nowlabel);
            nowlabel = null;
            return;
        }
        stmts.addElement(String.format("%s%s", Specification.PigletIndent, stmt));
    }

    public void setLabel(String label) {
        nowlabel = label;
    }

    public void addInitStmt(String stmt) {
        initStmts.addElement(String.format("%s%s", Specification.PigletIndent, stmt));
    }

    public void print(PrintStream out) {
        out.printf("%s [%d][%d][%d]\n", name, argnum, spillnum, maxcallarg);
        for (String stmt: initStmts) {
            out.printf("%s\n", stmt);
        }
        for (String stmt: stmts) {
            out.printf("%s\n", stmt);
        }
        out.printf("END\n\n");
    }
}

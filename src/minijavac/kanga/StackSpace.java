package minijavac.kanga;

import java.util.TreeSet;

public class StackSpace {
    private int top;
    private TreeSet<Integer> recyclePool;

    private void recycle(int t) {
        recyclePool.add(t);
    }

    public StackSpace() {
        top = 0;
        recyclePool = new TreeSet<>();
    }

    public int New() {
        if (recyclePool != null && !recyclePool.isEmpty()) {
            Integer u = recyclePool.first();
            recyclePool.remove(u);
            return u;
        }
        return top++;
    }
    public int NewTop() {
        return top++;
    }
    public int GetTop() {
        return top;
    }
    public void Recycle(int... units) {
        for (int u : units) {
            recycle(u);
        }
    }
}

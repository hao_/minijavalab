package minijavac.kanga.visitors;

import minijavac.ErrorHandler;
import minijavac.Specification;
import minijavac.kanga.JTBvisitor.GJNoArguDepthFirst;
import minijavac.kanga.parser.*;
import minijavac.mips.MIPSProgram;
import minijavac.mips.MIPSSymbol;

import java.util.Enumeration;

public class ToMIPSVisitor extends GJNoArguDepthFirst<String> {
    private MIPSProgram mipsProgram;
    private String nowlabel;
    private int retType, argNum, passArgNum;

    private static final int retTypeReg = 0;
    private static final int retTypeAddr = 1;
    private static final int retTypeNat = 2;
    private static final int retTypeOpr = 3;

    private ToMIPSVisitor(MIPSProgram _mipsProgram) {
        mipsProgram = _mipsProgram;
        nowlabel = null;
    }

    public static void VisitorEntry(Node goal, MIPSProgram mipsProgram) {
        goal.accept(new ToMIPSVisitor(mipsProgram));
    }

    private void setLabel(String label) {
        nowlabel = label;
    }
    private void addLine(String line) {
        mipsProgram.addLine(line);
    }
    private void addTabLine(String line) {
        mipsProgram.addLine(String.format("%s%s", Specification.PigletIndent, line));
    }
    private void addLabelLine(String line) {
        if (nowlabel != null)  {
            mipsProgram.addLine(String.format("%s%s", Specification.MIPSLabeledIndent(nowlabel), line));
            nowlabel = null;
        } else {
            addTabLine(line);
        }
    }

    private String getLabelString(Label n) {
        return n.f0.tokenImage;
    }
    private String getReg(Reg n) {
        return MIPSSymbol.RegFromKanga(n.f0.choice.toString());
    }
    private int getInt(IntegerLiteral n) {
        return Integer.valueOf(n.f0.toString());
    }

    private void addPredefineSymbol() {
        for (String codes: MIPSSymbol.PredefinedCodes) {
            addLine(codes);
        }
    }

    /**
     * f0 -> "MAIN"
     * f1 -> "["
     * f2 -> IntegerLiteral()
     * f3 -> "]"
     * f4 -> "["
     * f5 -> IntegerLiteral()
     * f6 -> "]"
     * f7 -> "["
     * f8 -> IntegerLiteral()
     * f9 -> "]"
     * f10 -> StmtList()
     * f11 -> "END"
     * f12 -> ( Procedure() )*
     * f13 -> <EOF>
     */
    public String visit(Goal n) {
        addTabLine(".text");
        addTabLine(String.format(".globl      %s", Specification.MIPSMainFuncName));
        addLine(String.format("%s:", Specification.MIPSMainFuncName));

        int frameSize = 8 + 4 * getInt(n.f5);
        passArgNum = getInt(n.f8);
        argNum = getInt(n.f2);
        if (passArgNum > 4) {
            passArgNum -= 4;
            frameSize += passArgNum * 4;
        } else {
            passArgNum = 0;
        }
        if (argNum > 4) {
            argNum -= 4;
        } else {
            argNum = 0;
        }

        addLabelLine("move $fp, $sp");
        addLabelLine(String.format("subu $sp, $sp, %d", frameSize));
        addLabelLine("sw $ra, -4($fp)");

        n.f10.accept(this);
        addLabelLine("lw $ra, -4($fp)");
        addLabelLine(String.format("addu $sp, $sp, %d", frameSize));
        addLabelLine("j $ra");
        addLine("");

        n.f12.accept(this);
        addPredefineSymbol();
        return "";
    }

    /**
     * f0 -> ( ( Label() )? Stmt() )*
     */
    public String visit(StmtList n) {
        for (Enumeration<Node> e = n.f0.elements(); e.hasMoreElements(); ) {
            NodeSequence n1 = (NodeSequence)e.nextElement();
            NodeOptional labeln = (NodeOptional)n1.elementAt(0);
            Node stmtn = n1.elementAt(1);

            if (labeln.present()) {
                String label = ((Label) labeln.node).f0.toString();
                setLabel(label);
            }
            stmtn.accept(this);
        }
        return "";
    }

    /**
     * f0 -> Label()
     * f1 -> "["
     * f2 -> IntegerLiteral()
     * f3 -> "]"
     * f4 -> "["
     * f5 -> IntegerLiteral()
     * f6 -> "]"
     * f7 -> "["
     * f8 -> IntegerLiteral()
     * f9 -> "]"
     * f10 -> StmtList()
     * f11 -> "END"
     */
    public String visit(Procedure n) {
        String funcName  = getLabelString(n.f0);
        addTabLine(".text");
        addTabLine(String.format(".globl       %s", funcName));
        addLine(String.format("%s:", funcName));

        int frameSize = 8 + 4 * getInt(n.f5);
        passArgNum = getInt(n.f8);
        argNum = getInt(n.f2);
        if (passArgNum > 4) {
            passArgNum -= 4;
            frameSize += passArgNum * 4;
        } else {
            passArgNum = 0;
        }
        if (argNum > 4) {
            argNum -= 4;
        } else {
            argNum = 0;
        }

        addLabelLine("sw $fp, -8($sp)");
        addLabelLine("move $fp, $sp");
        addLabelLine(String.format("subu $sp, $sp, %d", frameSize));
        addLabelLine("sw $ra, -4($fp)");

        n.f10.accept(this);

        addLabelLine("lw $ra, -4($fp)");
        addLabelLine(String.format("lw $fp, %d($sp)", frameSize - 8));
        addLabelLine(String.format("addu $sp, $sp, %d", frameSize));
        addLabelLine("j $ra");
        addLine("");
        return "";
    }

    /**
     * f0 -> NoOpStmt()
     *       | ErrorStmt()
     *       | CJumpStmt()
     *       | JumpStmt()
     *       | HStoreStmt()
     *       | HLoadStmt()
     *       | MoveStmt()
     *       | PrintStmt()
     *       | ALoadStmt()
     *       | AStoreStmt()
     *       | PassArgStmt()
     *       | CallStmt()
     */
    public String visit(Stmt n) {
        return n.f0.accept(this);
    }

    /**
     * f0 -> "NOOP"
     */
    public String visit(NoOpStmt n) {
        addLabelLine("nop");
        return "";
    }

    /**
     * f0 -> "ERROR"
     */
    public String visit(ErrorStmt n) {
        addLabelLine(String.format("j %s", MIPSSymbol.Func_error));
        return "";
    }

    /**
     * f0 -> "CJUMP"
     * f1 -> Reg()
     * f2 -> Label()
     */
    public String visit(CJumpStmt n) {
        addLabelLine(String.format("beqz %s %s",
                getReg(n.f1),
                getLabelString(n.f2)
        ));
        return "";
    }

    /**
     * f0 -> "JUMP"
     * f1 -> Label()
     */
    public String visit(JumpStmt n) {
        addLabelLine(String.format("b %s", getLabelString(n.f1)));
        return "";
    }

    /**
     * f0 -> "HSTORE"
     * f1 -> Reg()
     * f2 -> IntegerLiteral()
     * f3 -> Reg()
     */
    public String visit(HStoreStmt n) {
        addLabelLine(String.format("sw %s, %d(%s)", getReg(n.f3), getInt(n.f2), getReg(n.f1)));
        return "";
    }

    /**
     * f0 -> "HLOAD"
     * f1 -> Reg()
     * f2 -> Reg()
     * f3 -> IntegerLiteral()
     */
    public String visit(HLoadStmt n) {
        addLabelLine(String.format("lw %s, %d(%s)", getReg(n.f1), getInt(n.f3), getReg(n.f2)));
        return "";
    }

    /**
     * f0 -> "MOVE"
     * f1 -> Reg()
     * f2 -> Exp()
     */
    public String visit(MoveStmt n) {
        if (n.f2.f0.which == 1) {
            String ptn = n.f2.accept(this);
            addLabelLine(String.format(ptn, getReg(n.f1)));
        } else {
            String exp = n.f2.accept(this);
            switch (retType) {
                case retTypeReg:
                    addLabelLine(String.format("move %s %s", getReg(n.f1), exp));
                    break;
                case retTypeAddr:
                    addLabelLine(String.format("la %s %s", getReg(n.f1), exp));
                    break;
                case retTypeNat:
                    addLabelLine(String.format("li %s %s", getReg(n.f1), exp));
                    break;
                default:
                    ErrorHandler.MIPSError(String.format("Invalid retType = %d", retType));
            }
        }
        return "";
    }

    /**
     * f0 -> "PRINT"
     * f1 -> SimpleExp()
     */
    public String visit(PrintStmt n) {
        String exp = n.f1.accept(this);
        switch (retType) {
            case retTypeReg:
                addLabelLine(String.format("move $a0 %s", exp));
                break;
            case retTypeNat:
                addLabelLine(String.format("li $a0 %s", exp));
                break;
            default:
                ErrorHandler.MIPSError(String.format("Invalid retType = %d", retType));
        }

        addLabelLine(String.format("jal %s", MIPSSymbol.Func_print));
        return "";
    }

    /**
     * f0 -> "ALOAD"
     * f1 -> Reg()
     * f2 -> SpilledArg()
     */
    public String visit(ALoadStmt n) {
        addLabelLine(String.format("lw %s, %s", getReg(n.f1), n.f2.accept(this)));
        return "";
    }

    /**
     * f0 -> "ASTORE"
     * f1 -> SpilledArg()
     * f2 -> Reg()
     */
    public String visit(AStoreStmt n) {
        addLabelLine(String.format("sw %s, %s", getReg(n.f2), n.f1.accept(this)));
        return "";
    }

    /**
     * f0 -> "PASSARG"
     * f1 -> IntegerLiteral()
     * f2 -> Reg()
     */
    public String visit(PassArgStmt n) {
        addLabelLine(String.format("sw %s %d($sp)", getReg(n.f2), (getInt(n.f1) - 1) * 4));
        return "";
    }

    /**
     * f0 -> "CALL"
     * f1 -> SimpleExp()
     */
    public String visit(CallStmt n) {
        String exp = n.f1.accept(this);
        String command;
        switch (retType) {
            case retTypeReg:
                command = "jalr";
                break;
            case retTypeAddr:
                command = "jal";
                break;
            default:
                ErrorHandler.MIPSError(String.format("Invalid retType = %d\n", retType));
                command = "";
        }
        addLabelLine(String.format("%s %s", command, exp));
        return "";
    }

    /**
     * f0 -> HAllocate()
     *       | BinOp()
     *       | SimpleExp()
     */
    public String visit(Exp n) {
        return n.f0.accept(this);
    }

    /**
     * f0 -> "HALLOCATE"
     * f1 -> SimpleExp()
     */
    public String visit(HAllocate n) {
        String exp = n.f1.accept(this);
        switch (retType) {
            case retTypeNat:
                addLabelLine(String.format("li $a0 %s", exp));
                break;
            case retTypeReg:
                addLabelLine(String.format("move $a0 %s", exp));
                break;
            default:
                ErrorHandler.MIPSError(String.format("Invalid retType = %d", retType));
        }

        addLabelLine(String.format("jal %s", MIPSSymbol.Func_halloc));
        retType = retTypeReg;
        return "$v0";
    }

    /**
     * f0 -> Operator()
     * f1 -> Reg()
     * f2 -> SimpleExp()
     */
    public String visit(BinOp n) {
        return String.format("%s %%s, %s, %s", n.f0.accept(this), n.f1.accept(this), n.f2.accept(this));
    }

    /**
     * f0 -> "LT"
     *       | "PLUS"
     *       | "MINUS"
     *       | "TIMES"
     */
    public String visit(Operator n) {
        String op = n.f0.choice.toString();
        String retOp;
        switch (op) {
            case "PLUS":
                retOp = "add";
                break;
            case "MINUS":
                retOp = "sub";
                break;
            case "TIMES":
                retOp = "mul";
                break;
            case "LT":
                retOp = "slt";
                break;
            default:
                ErrorHandler.MIPSError(String.format("Invalid operator %s", op));
                retOp = "";
        }
        retType = retTypeOpr;
        return retOp;
    }

    /**
     * f0 -> "SPILLEDARG"
     * f1 -> IntegerLiteral()
     */
    public String visit(SpilledArg n) {
        int k = getInt(n.f1);
        if (k < argNum) {
            return String.format("%d($fp)", 4 * k);
        }
        return String.format("%d($sp)", (k - argNum + passArgNum) * 4);
    }

    /**
     * f0 -> Reg()
     *       | IntegerLiteral()
     *       | Label()
     */
    public String visit(SimpleExp n) {
        return n.f0.accept(this);
    }

    /**
     * f0 -> "a0"
     *       | "a1"
     *       | "a2"
     *       | "a3"
     *       | "t0"
     *       | "t1"
     *       | "t2"
     *       | "t3"
     *       | "t4"
     *       | "t5"
     *       | "t6"
     *       | "t7"
     *       | "s0"
     *       | "s1"
     *       | "s2"
     *       | "s3"
     *       | "s4"
     *       | "s5"
     *       | "s6"
     *       | "s7"
     *       | "t8"
     *       | "t9"
     *       | "v0"
     *       | "v1"
     */
    public String visit(Reg n) {
        retType = retTypeReg;
        return getReg(n);
    }

    /**
     * f0 -> <INTEGER_LITERAL>
     */
    public String visit(IntegerLiteral n) {
        retType = retTypeNat;
        return n.f0.toString();
    }

    /**
     * f0 -> <IDENTIFIER>
     */
    public String visit(Label n) {
        retType = retTypeAddr;
        return n.f0.toString();
    }
}

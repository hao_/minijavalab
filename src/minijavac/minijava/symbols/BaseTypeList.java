package minijavac.minijava.symbols;

public class BaseTypeList {
    public static MBaseType mvoid = new MBaseType("void", 4);
    public static MBaseType mint = new MBaseType("int", 4);
    public static MBaseType mboolean = new MBaseType("boolean", 4);
    public static MBaseType marraytype = new MBaseType("int[]", 4);
    public static MBaseType mArgs = new MBaseType("String[]", 4);
    public static MBaseType mUnit = new MBaseType("!!Unit", 0);

    public static MClass GetBaseTypeClass(String name) {
        if (name == mvoid.name) {
            return mvoid;
        } else if (name == mint.name) {
            return mint;
        } else if (name == mboolean.name) {
            return mboolean;
        } else if (name == marraytype.name) {
            return marraytype;
        } else if (name == mArgs.name) {
            return mArgs;
        } else {
            return null;
        }
    }
}

package minijavac.minijava.symbols;

public class MBaseType extends MClass {
    private int baseTypesize;

    MBaseType(String _name, int size) {
        super(_name);
        baseTypesize = size;
    }

    @Override
    public boolean ReceiveAssignment(MClass a) {
        return super.IsSameClass(a);
    }

    public int size() {
        return baseTypesize;
    }
}

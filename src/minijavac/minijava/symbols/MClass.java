package minijavac.minijava.symbols;

public class MClass extends MSymbol {

    MClass(String _name) { super(_name); }

    public boolean IsSameClass(MClass classB) {
        return name == classB.name;
    }

    public boolean ReceiveAssignment(MClass a) {
        return false;
    }
}

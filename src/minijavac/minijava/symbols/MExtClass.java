package minijavac.minijava.symbols;

import javafx.util.Pair;
import minijavac.ErrorHandler;
import minijavac.Specification;

import java.util.HashMap;
import java.util.Map;

public class MExtClass extends MClass implements SymbolSaver {
    private MExtClass superClass;
    private MVar thisVar;

    private HashMap<String, MMethod> methods; // Always override parent methods
    private HashMap<String, MVar> memberVariables;
    private MSymbolTable symbolTable;
    // Static variables: not implemented

    private boolean checkOverrideMethod(MMethod method) {
        MExtClass ancestor = superClass;
        while (ancestor != null) {
            if (ancestor.methods.containsKey(method.name)) {

                if (!ancestor.methods.get(method.name).IsSameParameterList(method)) {
                    return false;
                }
            }
            ancestor = ancestor.superClass;
        }
        return true;
    }

    public Scope scope;

    @Override
    public boolean ReceiveAssignment(MClass c) {
        if (!(c instanceof MExtClass)) return false;

        /*
        System.err.println("CHECK TYPE");
        System.err.println(FullSymbolName());
        System.err.println(c.FullSymbolName());*/


        MExtClass ec = (MExtClass)c;
        while (ec != null) {
            if (ec.IsSameClass(this)) {
                return true;
            }
            ec = ec.superClass;
        }
        return false;
    }

    public void Inherit(MExtClass _superClass) {
        // Check circulation
        MExtClass fa = _superClass;
        while (fa != null) {
            if (fa.IsSameClass(this)) {
                ErrorHandler.TypeCheckError(FullSymbolName() + ": inheritance cycle.");
            }
            fa = fa.superClass;
        }

        assert scope.parentScope == null;
        superClass = _superClass;
        /*
        System.err.println(FullSymbolName());
        System.err.println("Extends");
        System.err.println(superClass.FullSymbolName());*/

        scope.parentScope = _superClass.scope;
    }

    public MExtClass(String _name, MSymbolTable sTable) {
        super(_name);

        // Inheritance handled in Inherit()
        superClass = null;

        methods = new HashMap<String, MMethod>();
        symbolTable = sTable;
        scope = new Scope(null, symbolTable, this);
        memberVariables = new HashMap<String, MVar>();

        // Register special identifier
        thisVar = new MVar("this", this);
        scope.AddVars(thisVar, this);

        // Layout, should init once
        instanceTableLayout = null;
        instanceTableLayoutInited = false;
    }

    public void AddMemberVariable(MVar var) {
        /*
        // Check conflict between method name and variable name
        if (methods.containsKey(obj.name)) {
            ErrorHandler.TypeCheckError(FullSymbolName() + ": member variable name conflicts with method name.");
        }
        */
        scope.AddVars(var, this);
        memberVariables.put(var.name, var);
    }

    public MVar GetMemberVariables(String name) {
        if (memberVariables.containsKey(name)) {
            return memberVariables.get(name);
        } else {
            if (superClass != null)
                return superClass.GetMemberVariables(name);
            else
                return null;
        }
    }

    public void AddMethod(MMethod method) {

        if (!checkOverrideMethod(method)) {
            assert false;
            ErrorHandler.TypeCheckError(method.FullSymbolName() + ": overload of method is not allowed.");
        }

        /*
        // Check conflict between method name and variable name
        if (scope.ContainVars(method.name)) {
            ErrorHandler.TypeCheckError(method.FullSymbolName() + ": method variable name conflicts with member name.");
        }*/

        if (methods.containsKey(method.name)) {
            ErrorHandler.TypeCheckError(method.FullSymbolName() + ": redefinition of methods.");
        } else {
            methods.put(method.name, method);
        }
    }

    public MMethod GetMethod(String name) {
        if (methods.containsKey(name)) {
            return methods.get(name);
        } else {
            if (superClass != null)
                return superClass.GetMethod(name);
            else
                return null;
        }
    }

    public MSymbolTable GetSymbolTable() {
        return symbolTable;
    }

    public MSymbol lookup(String name) {
        return scope.GetVar(name);
    }

    @Override
    public Scope GetScope() {
        return scope;
    }

    // Memory Layout of the instance of this class

    private class InstanceTableLayout {
        public int size, methodTableSize;
        public HashMap<String, Integer> fieldOff;
        public HashMap<String, Pair<String, Integer> > methodOff;

        public InstanceTableLayout() {
            size = methodTableSize = 0;
            fieldOff = new HashMap<>();
            methodOff = new HashMap<>();
        }
    };

    private InstanceTableLayout instanceTableLayout;
    public final static int layoutMethodTableOff = 0;
    boolean instanceTableLayoutInited;

    public void InitInstanceTable() {
        if (instanceTableLayoutInited) {
            return;
        }
        instanceTableLayout = new InstanceTableLayout();

        // Build instance table
        int base = Specification.AddrSize;
        if (superClass != null) {
            superClass.InitInstanceTable();
            base = superClass.layoutSize();
        }
        for (Map.Entry<String, MVar> entry : memberVariables.entrySet()) {
            String name = entry.getKey();
            MVar var = entry.getValue();
            assert !instanceTableLayout.fieldOff.containsKey(name);
            instanceTableLayout.fieldOff.put(name, base);
            base += var.VarSize();
        }
        instanceTableLayout.size = base;

        // Build method table
        int methodTableBase = 0;
        if (superClass != null) {
            methodTableBase = superClass.MethodTableSize();
            for (Map.Entry<String, Pair<String, Integer> > entry : superClass.instanceTableLayout.methodOff.entrySet()) {
                instanceTableLayout.methodOff.put(entry.getKey(), entry.getValue());
            }
        }
        for (Map.Entry<String, MMethod> entry : methods.entrySet()) {
            String name = entry.getKey();
            if (!instanceTableLayout.methodOff.containsKey(name)) {
                instanceTableLayout.methodOff.put(name,
                        new Pair<>(Specification.PigletFuncName(this.name, name), methodTableBase)
                );
                methodTableBase += Specification.AddrSize;
            } else {
                Integer off = instanceTableLayout.methodOff.get(name).getValue();
                instanceTableLayout.methodOff.put(name,
                        new Pair<>(Specification.PigletFuncName(this.name, name), off)
                );
            }
        }
        instanceTableLayout.methodTableSize = methodTableBase;

        instanceTableLayoutInited = true;
    }

    public int layoutSize() {
        assert instanceTableLayoutInited;
        return instanceTableLayout.size;
    }
    public int MethodTableSize() {
        assert instanceTableLayoutInited;
        return instanceTableLayout.methodTableSize;
    }
    public int layoutFieldOff(String field) {
        assert instanceTableLayoutInited;
        for (MExtClass cur = this; cur != null; cur = cur.superClass) {
            if (cur.instanceTableLayout.fieldOff.containsKey(field)) {
                return cur.instanceTableLayout.fieldOff.get(field);
            }
        }
        ErrorHandler.PigletError(String.format("field %s not exists in MExtClass.layoutFiledOff", field));
        return -1;
    }
    // Return Pair<methodTableOffset, methodOffset in method table>
    public Pair<String, Integer> layoutMethodOff(String method) {
        assert instanceTableLayoutInited;
        for (MExtClass cur = this; cur != null; cur = cur.superClass) {
            if (cur.instanceTableLayout.methodOff.containsKey(method)) {
                return cur.instanceTableLayout.methodOff.get(method);
            }
        }
        ErrorHandler.PigletError(String.format("method %s not exists in MExtClass.layoutMethodOff", method));
        return null;
    }

    public HashMap<String, Pair<String, Integer>> AllMethodOff() {
        return instanceTableLayout.methodOff;
    }
}
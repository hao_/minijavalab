package minijavac.minijava.symbols;

import minijavac.ErrorHandler;


import java.util.Vector;

import static minijavac.Specification.MaxMethodParametersNum;

public class MMethod extends MSymbol implements SymbolSaver {
    private MExtClass owner; // Only ExtClass can have methods
    public Vector<MVar> parameterList;
    private MClass returnType;

    public Scope scope; // scope that stores both static and local variables

    @Override
    public String FullSymbolName() { return owner.FullSymbolName() + "." + name; }


    public MMethod(String _name, MExtClass _owner, MClass _returnType) {
        super(_name);
        //if (_owner != null) {
        owner = _owner;
        scope = new Scope(_owner.scope, GetSymbolTable(), owner);
        //}
        parameterList = new Vector<MVar>();
        returnType = _returnType;
    }


    public void AddParameter(MVar var, boolean checkName) {
        if (parameterList.size() + 1 > MaxMethodParametersNum) {
            ErrorHandler.TypeCheckError(FullSymbolName() + ": too much parameters in method.");
        }

        if (checkName) {
            int size = parameterList.size();
            for (int i = 0; i < size; i++)
                if (parameterList.get(i).name == var.name) {
                    ErrorHandler.TypeCheckError(FullSymbolName() + ": same parameters' name.");
                }

            scope.AddVars(var, this);
        }

        parameterList.addElement(var);
    }

    public boolean IsSameParameterList(MMethod methodB) {
        if (!returnType.ReceiveAssignment(methodB.returnType))
            return false;

        int len = parameterList.size();
        if (len != methodB.parameterList.size())
            return false;

        for (int i = 0; i < len; i++)
            if (!parameterList.get(i).ReceiveAssignment(methodB.parameterList.get(i))) {
                return false;
            }

        return true;
    }

    public MSymbolTable GetSymbolTable() {
        return owner.GetSymbolTable();
    }

    public boolean IsReturnTyped(MClass type) {
        return returnType.IsSameClass(type);
    }

    public MClass GetReturnType() {
        return returnType;
    }

    public MExtClass GetOwner() {
        return owner;
    }

    @Override
    public MSymbol lookup(String name) {
        return scope.GetVar(name);
    }

    @Override
    public Scope GetScope() {
        return scope;
    }
}

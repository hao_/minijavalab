package minijavac.minijava.symbols;

public class MSymbol implements VisitorAccepter {
    public String name;

    public MSymbol(String _name) {
        name = _name;
    }

    public String FullSymbolName() {
        return name;
    }
}

package minijavac.minijava.symbols;

import minijavac.ErrorHandler;

import java.util.HashMap;
import java.util.Map;

public class MSymbolTable implements SymbolSaver {
    private MClass mainClass;
    private HashMap<String, MClass> classList;
    private Scope globalScope;

    public MSymbolTable() {
        mainClass = null;
        classList = new HashMap<String, MClass>();
        globalScope = null;
    }

    public MClass GetClass(String name) {
        // Need to add BaseType
        MClass base = BaseTypeList.GetBaseTypeClass(name);
        if (base != null) {
            return base;
        }
        if (classList.containsKey(name)) {
            return classList.get(name);
        } else {
            return null;
        }
    }

    public void AddClass(MClass mclass, boolean isMain) {
        if (classList.containsKey(mclass.name)) {
            ErrorHandler.TypeCheckError(mclass.FullSymbolName() + ": redefinition of class.");
        } else {
            classList.put(mclass.name, mclass);
        }

        if (isMain) {
            if (mainClass != null) {
                ErrorHandler.TypeCheckError(mclass.FullSymbolName() + ": multiple main class.");
            }
            mainClass = mclass;
        }
    }

    public MSymbol lookup(String name) {
        return GetClass(name);
    }

    public MSymbolTable GetSymbolTable() {
        return this;
    }

    @Override
    public Scope GetScope() {
        return globalScope;
    }

    public void InitClassLayout() {
        for (Map.Entry<String, MClass> entry: classList.entrySet()) {
            MClass c = entry.getValue();
            if (!(c instanceof MExtClass)) {
                continue;
            }
            ((MExtClass) c).InitInstanceTable();
        }
    }
}

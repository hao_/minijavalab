package minijavac.minijava.symbols;

import minijavac.ErrorHandler;
import minijavac.Specification;

public class MVar extends MSymbol {
    public MClass type;


    public MVar(String _name, MClass _type) {
        super(_name);
        type = _type;
    }

    public boolean IsSameType(MVar varB) {
        return type.IsSameClass(varB.type);
    }
    public boolean ReceiveAssignment(MVar varB) { return type.ReceiveAssignment(varB.type); }

    public int VarSize() {
        if (type instanceof MExtClass) {
            return Specification.AddrSize;
        } else if (type instanceof MBaseType){
            return ((MBaseType) type).size();
        } else {
            ErrorHandler.UnknownError("unknown var type in MVar.VarSize()");
            return 0;
        }
    }
}

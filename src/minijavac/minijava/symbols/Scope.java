package minijavac.minijava.symbols;

import java.util.HashMap;
import minijavac.ErrorHandler;

public class Scope implements SymbolSaver {
    private HashMap<String, MVar> vars;
    public Scope parentScope;
    private MSymbolTable symbolTable;
    private MExtClass owner;



    public Scope(Scope _parentScope, MSymbolTable _symbolTable, MExtClass _owner) {
        vars = new HashMap<String, MVar>();
        parentScope = _parentScope;
        symbolTable = _symbolTable;
        owner = _owner;
    }

    /*
    public void AddVars(MVar var) {
        if (vars.containsKey(var.name)) {
            ErrorHandler.TypeCheckError("_UnknownScope: redefinition of variables.");
        } else {
            vars.put(var.name, var);
        }
    }*/

    public void AddVars(MVar var, MSymbol symbol) {
        if (vars.containsKey(var.name)) {
            ErrorHandler.TypeCheckError(var.FullSymbolName() + ": redefinition of variables.");
        } else {
            vars.put(var.name, var);
        }
    }

    public boolean ContainVars(String name) {
        return vars.containsKey(name);
    }

    public void CopyFromScope(Scope s) {
        parentScope = s;
        /*
        for(HashMap.Entry<String, MVar> entry: s.vars.entrySet()) {

            if (!vars.containsKey(entry.getKey())) {
                vars.put(entry.getKey(), entry.getValue());
            }
        }*/
    }

    public MVar GetVar(String name) {

        Scope nowScope = this;
        while (nowScope != null) {
            if (nowScope.vars.containsKey(name)) {
                return nowScope.vars.get(name);
            }
            nowScope = nowScope.parentScope;
        }
        return null;
    }

    public MExtClass GetOwner() {
        return owner;
    }

    public MSymbol lookup(String name) {
        return GetVar(name);
    }
    public MSymbolTable GetSymbolTable() {
        return symbolTable;
    }
    public Scope GetScope() {
        return this;
    }
}

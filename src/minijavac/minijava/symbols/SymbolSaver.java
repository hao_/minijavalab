package minijavac.minijava.symbols;

public interface SymbolSaver extends VisitorAccepter {
    public MSymbol lookup(String name);
    public MSymbolTable GetSymbolTable();
    public Scope GetScope();
}

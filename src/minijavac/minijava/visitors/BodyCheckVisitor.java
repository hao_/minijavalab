package minijavac.minijava.visitors;

import minijavac.ErrorHandler;
import minijavac.minijava.JTBvisitor.GJDepthFirst;
import minijavac.minijava.symbols.*;
import minijavac.minijava.syntaxtree.*;

public class BodyCheckVisitor extends BuildSymbolTableVisitor {
    /**
     * f0 -> "class"
     * f1 -> Identifier()
     * f2 -> "{"
     * f3 -> "public"
     * f4 -> "static"
     * f5 -> "void"
     * f6 -> "main"
     * f7 -> "("
     * f8 -> "String"
     * f9 -> "["
     * f10 -> "]"
     * f11 -> Identifier()
     * f12 -> ")"
     * f13 -> "{"
     * f14 -> ( VarDeclaration() )*
     * f15 -> ( Statement() )*
     * f16 -> "}"
     * f17 -> "}"
     */
    public VisitorAccepter visit(MainClass n, SymbolSaver argu) {
        MSymbolTable symbolTable = (MSymbolTable)argu;
        String className = n.f1.f0.tokenImage;

        MExtClass mc = (MExtClass) symbolTable.GetClass(className);
        MMethod main = mc.GetMethod(n.f6.tokenImage);


        n.f14.accept(this, main);
        n.f15.accept(this, main.scope);

        return symbolTable;
    }

    /**
     * f0 -> "class"
     * f1 -> Identifier()
     * f2 -> "{"
     * f3 -> ( VarDeclaration() )*
     * f4 -> ( MethodDeclaration() )*
     * f5 -> "}"
     */
    public VisitorAccepter visit(ClassDeclaration n, SymbolSaver argu) {
        String className = n.f1.f0.tokenImage;
        MExtClass ec = (MExtClass)argu.lookup(className);

        //n.f3.accept(this, ec); Do not declare again
        n.f4.accept(this, ec);

        return null;
    }

    /**
     * f0 -> "class"
     * f1 -> Identifier()
     * f2 -> "extends"
     * f3 -> Identifier()
     * f4 -> "{"
     * f5 -> ( VarDeclaration() )*
     * f6 -> ( MethodDeclaration() )*
     * f7 -> "}"
     */
    public VisitorAccepter visit(ClassExtendsDeclaration n, SymbolSaver argu) {
        String className = n.f1.f0.tokenImage;
        MExtClass ec = (MExtClass)argu.lookup(className);

        //n.f5.accept(this, ec); //Do not declare again
        n.f6.accept(this, ec);

        return null;
    }

    /**
     * f0 -> Type()
     * f1 -> Identifier()
     * f2 -> ";"
     */
    public VisitorAccepter visit(VarDeclaration n, SymbolSaver argu) {
        MClass type = (MClass)n.f0.accept(this, argu.GetSymbolTable());
        assert type != null;

        argu.GetScope().AddVars(new MVar(n.f1.f0.tokenImage, type), (MSymbol)argu);
        return null;
    }

    /*
     * f0 -> "public"
     * f1 -> Type()
     * f2 -> Identifier()
     * f3 -> "("
     * f4 -> ( FormalParameterList() )?
     * f5 -> ")"
     * f6 -> "{"
     * f7 -> ( VarDeclaration() )*
     * f8 -> ( Statement() )*
     * f9 -> "return"
     * f10 -> Expression()
     * f11 -> ";"
     * f12 -> "}"
     */
    public VisitorAccepter visit(MethodDeclaration n, SymbolSaver argu) {
        MExtClass ec = (MExtClass)argu;
        MMethod method = ec.GetMethod(n.f2.f0.tokenImage);

        n.f7.accept(this, method);
        n.f8.accept(this, method.scope);

        MClass type = (MClass) n.f10.accept(this, method.scope);

        // Check return type
        if (!method.IsReturnTyped(type)) {
            ErrorHandler.TypeCheckError(method.FullSymbolName() + ": return type mismatches.");
        }

        return null;
    }

    /**
     * f0 -> "{"
     * f1 -> ( Statement() )*
     * f2 -> "}"
     */
    public VisitorAccepter visit(Block n, SymbolSaver argu) {
        Scope scope = new Scope(argu.GetScope(), argu.GetSymbolTable(), argu.GetScope().GetOwner());
        n.f1.accept(this, scope);
        return BaseTypeList.mUnit;
    }

    /**
     * f0 -> Identifier()
     * f1 -> "="
     * f2 -> Expression()
     * f3 -> ";"
     */
    public VisitorAccepter visit(AssignmentStatement n, SymbolSaver argu) {
        MVar var = (MVar)argu.lookup(n.f0.f0.tokenImage);
        MClass type = (MClass)n.f2.accept(this, argu);

        try {
            if (!var.type.ReceiveAssignment(type)) {
                ErrorHandler.TypeCheckError(var.FullSymbolName() + ": be assigned with an unidentical type.");
            }
        }
        catch (NullPointerException e) {
            ErrorHandler.TypeCheckError(n.f0.f0.tokenImage + ": identifier not exists.");
        }
        return BaseTypeList.mUnit;
    }

    /**
     * f0 -> Identifier()
     * f1 -> "["
     * f2 -> Expression()
     * f3 -> "]"
     * f4 -> "="
     * f5 -> Expression()
     * f6 -> ";"
     */
    public VisitorAccepter visit(ArrayAssignmentStatement n, SymbolSaver argu) {
        MVar var = (MVar)argu.lookup(n.f0.f0.tokenImage);

        try {
            if (!var.type.IsSameClass(BaseTypeList.marraytype)) {
                ErrorHandler.TypeCheckError(var.FullSymbolName() + ": not Arraytype.");
            }
        } catch (NullPointerException e) {
            ErrorHandler.TypeCheckError(n.f0.f0.tokenImage + ": identifier not exists.");
        }

        MClass type_exp2 = (MClass)n.f2.accept(this, argu);
        MClass type_exp5 = (MClass)n.f5.accept(this, argu);

        if (!type_exp2.IsSameClass(BaseTypeList.mint)) {
            ErrorHandler.TypeCheckError(var.FullSymbolName() + ": array subscript not int type.");
        }
        if (!type_exp5.IsSameClass(BaseTypeList.mint)) {
            ErrorHandler.TypeCheckError(var.FullSymbolName() + ": array object not int type.");
        }

        return BaseTypeList.mUnit;
    }

    /**
     * f0 -> "if"
     * f1 -> "("
     * f2 -> Expression()
     * f3 -> ")"
     * f4 -> Statement()
     * f5 -> "else"
     * f6 -> Statement()
     */
    public VisitorAccepter visit(IfStatement n, SymbolSaver argu) {

        MClass type_exp2 = (MClass) n.f2.accept(this, argu);
        if (!type_exp2.IsSameClass(BaseTypeList.mboolean))
            ErrorHandler.TypeCheckError("Expression in if should be with boolean type.");

        n.f4.accept(this, argu);
        n.f6.accept(this, argu);
        return BaseTypeList.mUnit;
    }
    /**
     * f0 -> "while"
     * f1 -> "("
     * f2 -> Expression()
     * f3 -> ")"
     * f4 -> Statement()
     */
    public VisitorAccepter visit(WhileStatement n, SymbolSaver argu) {

        MClass type_exp2 = (MClass)n.f2.accept(this, argu);
        if (!type_exp2.IsSameClass(BaseTypeList.mboolean))
            ErrorHandler.TypeCheckError("Expression in while should be with boolean type.");

        n.f4.accept(this, argu);
        return BaseTypeList.mUnit;
    }

    /**
     * f0 -> "System.out.println"
     * f1 -> "("
     * f2 -> Expression()
     * f3 -> ")"
     * f4 -> ";"
     */
    public VisitorAccepter visit(PrintStatement n, SymbolSaver argu) {
        MClass type_exp2 = (MClass)n.f2.accept(this, argu);

        if (!type_exp2.IsSameClass(BaseTypeList.mint))
            ErrorHandler.TypeCheckError("Expression in System.out.println should be with int type.");
        return BaseTypeList.mUnit;
    }

    /**
     * f0 -> AndExpression()
     *       | CompareExpression()
     *       | PlusExpression()
     *       | MinusExpression()
     *       | TimesExpression()
     *       | ArrayLookup()
     *       | ArrayLength()
     *       | MessageSend()
     *       | PrimaryExpression()
     */
    public VisitorAccepter visit(Expression n, SymbolSaver argu) {
        return n.f0.accept(this, argu);
    }

    /**
     * f0 -> PrimaryExpression()
     * f1 -> "&&"
     * f2 -> PrimaryExpression()
     */
    public VisitorAccepter visit(AndExpression n, SymbolSaver argu) {
        MClass type_exp0 = (MClass)n.f0.accept(this, argu);
        MClass type_exp2 = (MClass)n.f2.accept(this, argu);
        if (!type_exp2.IsSameClass(BaseTypeList.mboolean)
                || !type_exp0.IsSameClass(BaseTypeList.mboolean))
            ErrorHandler.TypeCheckError("Expression around && should be with boolean type.");
        return BaseTypeList.mboolean;
    }

    /**
     * f0 -> PrimaryExpression()
     * f1 -> "<"
     * f2 -> PrimaryExpression()
     */
    public VisitorAccepter visit(CompareExpression n, SymbolSaver argu) {
        MClass type_exp0 = (MClass)n.f0.accept(this, argu);
        MClass type_exp2 = (MClass)n.f2.accept(this, argu);
        if (!type_exp2.IsSameClass(BaseTypeList.mint)
                || !type_exp0.IsSameClass(BaseTypeList.mint))
            ErrorHandler.TypeCheckError("Expression around < should be with int type.");
        return BaseTypeList.mboolean;
    }
    /**
     * f0 -> PrimaryExpression()
     * f1 -> "+"
     * f2 -> PrimaryExpression()
     */
    public VisitorAccepter visit(PlusExpression n, SymbolSaver argu) {
        MClass type_exp0 = (MClass)n.f0.accept(this, argu);
        MClass type_exp2 = (MClass)n.f2.accept(this, argu);
        if (!type_exp2.IsSameClass(BaseTypeList.mint)
                || !type_exp0.IsSameClass(BaseTypeList.mint))
            ErrorHandler.TypeCheckError("Expression around + should be with int type.");
        return BaseTypeList.mint;
    }
    /**
     * f0 -> PrimaryExpression()
     * f1 -> "-"
     * f2 -> PrimaryExpression()
     */
    public VisitorAccepter visit(MinusExpression n, SymbolSaver argu) {
        MClass type_exp0 = (MClass)n.f0.accept(this, argu);
        MClass type_exp2 = (MClass)n.f2.accept(this, argu);
        if (!type_exp2.IsSameClass(BaseTypeList.mint)
                || !type_exp0.IsSameClass(BaseTypeList.mint))
            ErrorHandler.TypeCheckError("Expression around - should be with int type.");
        return BaseTypeList.mint;
    }
    /**
     * f0 -> PrimaryExpression()
     * f1 -> "*"
     * f2 -> PrimaryExpression()
     */
    public VisitorAccepter visit(TimesExpression n, SymbolSaver argu) {
        MClass type_exp0 = (MClass)n.f0.accept(this, argu);
        MClass type_exp2 = (MClass)n.f2.accept(this, argu);
        if (!type_exp2.IsSameClass(BaseTypeList.mint)
                || !type_exp0.IsSameClass(BaseTypeList.mint))
            ErrorHandler.TypeCheckError("Expression around * should be with int type.");
        return BaseTypeList.mint;
    }

    /**
     * f0 -> PrimaryExpression()
     * f1 -> "["
     * f2 -> PrimaryExpression()
     * f3 -> "]"
     */
    public VisitorAccepter visit(ArrayLookup n, SymbolSaver argu) {
        MClass type_exp0 = (MClass)n.f0.accept(this, argu);
        MClass type_exp2 = (MClass)n.f2.accept(this, argu);
        if (!type_exp0.IsSameClass(BaseTypeList.marraytype))
            ErrorHandler.TypeCheckError("ArrayLookup for a object which is not Arraytype.");
        if (!type_exp2.IsSameClass(BaseTypeList.mint))
            ErrorHandler.TypeCheckError("Array subscript should be with boolean type.");
        return BaseTypeList.mint;
    }
    /**
     * f0 -> PrimaryExpression()
     * f1 -> "."
     * f2 -> "length"
     */
    public VisitorAccepter visit(ArrayLength n, SymbolSaver argu) {
        MClass type_exp0 = (MClass)n.f0.accept(this, argu);
        MClass type_exp2 = (MClass)n.f2.accept(this, argu);
        if (!type_exp0.IsSameClass(BaseTypeList.marraytype))
            ErrorHandler.TypeCheckError("length() for a object which is not Arraytype.");
        return BaseTypeList.mint;
    }

    /**
     * f0 -> PrimaryExpression()
     * f1 -> "."
     * f2 -> Identifier()
     * f3 -> "("
     * f4 -> ( ExpressionList() )?
     * f5 -> ")"
     */
    public VisitorAccepter visit(MessageSend n, SymbolSaver argu) {
        Scope scope = (Scope)argu;
        MClass cst = (MClass)n.f0.accept(this, argu);

        MExtClass c = null;
        try {
            c = (MExtClass)cst;
        } catch (ClassCastException e) {
            ErrorHandler.TypeCheckError(cst.FullSymbolName() +
                    ": is not a class, could not messageSend.");
        }
        MMethod method = c.GetMethod(n.f2.f0.tokenImage);

        MMethod arg = null;
        try {
            arg = new MMethod("", scope.GetOwner(), method.GetReturnType());
        } catch (NullPointerException e) {
            ErrorHandler.TypeCheckError(n.f2.f0.tokenImage + ": method not exists");
        }

        arg.scope.CopyFromScope(scope);

        n.f4.accept(this, arg);

        if (!method.IsSameParameterList(arg)) {
            System.err.println("Need: ");
            for (MVar i: method.parameterList)
                System.err.println(i.type.FullSymbolName());
            System.err.println("Got: ");
            for (MVar i: arg.parameterList)
                System.err.println(i.type.FullSymbolName());
            ErrorHandler.TypeCheckError(method.FullSymbolName() +
                    ": wrong parameters list.");
        }

        return method.GetReturnType();
    }

    /**
     * f0 -> Expression()
     * f1 -> ( ExpressionRest() )*
     */
    public VisitorAccepter visit(ExpressionList n, SymbolSaver argu) {
        MMethod arg = (MMethod)argu;
        MClass c = (MClass)n.f0.accept(this, arg.scope);

        arg.AddParameter( new MVar("*", c) , false );

        n.f1.accept(this, arg);
        return null;
    }

    /**
     * f0 -> ","
     * f1 -> Expression()
     */
    public VisitorAccepter visit(ExpressionRest n, SymbolSaver argu) {
        MMethod arg = (MMethod)argu;
        MClass c = (MClass)n.f1.accept(this, arg.scope);

        arg.AddParameter( new MVar("*", c) , false );
        return null;
    }

    /**
     * f0 -> IntegerLiteral()
     *       | TrueLiteral()
     *       | FalseLiteral()
     *       | Identifier()
     *       | ThisExpression()
     *       | ArrayAllocationExpression()
     *       | AllocationExpression()
     *       | NotExpression()
     *       | BracketExpression()
     */
    public VisitorAccepter visit(PrimaryExpression n, SymbolSaver argu) {
        return n.f0.accept(this, argu);
    }

    /**
     * f0 -> <INTEGER_LITERAL>
     */
    public VisitorAccepter visit(IntegerLiteral n, SymbolSaver argu) {
        return BaseTypeList.mint;
    }

    /**
     * f0 -> "true"
     */
    public VisitorAccepter visit(TrueLiteral n, SymbolSaver argu) {
        return BaseTypeList.mboolean;
    }

    /**
     * f0 -> "false"
     */
    public VisitorAccepter visit(FalseLiteral n, SymbolSaver argu) {
        return BaseTypeList.mboolean;
    }
    /**
     * f0 -> <IDENTIFIER>
     */
    public VisitorAccepter visit(Identifier n, SymbolSaver argu) {
        //System.err.println(argu.GetScope().GetOwner().FullSymbolName());
        //System.err.println(argu.lookup("new_node"));

        MSymbol symbol = argu.lookup(n.f0.tokenImage);
        if (symbol == null) {
            ErrorHandler.TypeCheckError(n.f0.tokenImage + ": symbol not exists in scope.");
        }
        if (symbol instanceof MVar) {
            assert ((MVar)symbol).type != null;
            return ((MVar)symbol).type;
        }

        return symbol;
    }

    /**
     * f0 -> "this"
     */
    public VisitorAccepter visit(ThisExpression n, SymbolSaver argu) {
        return ((Scope)argu).GetOwner();
    }

    /**
     * f0 -> "new"
     * f1 -> "int"
     * f2 -> "["
     * f3 -> Expression()
     * f4 -> "]"
     */
    public VisitorAccepter visit(ArrayAllocationExpression n, SymbolSaver argu) {
        MClass exp_3 = (MClass) n.f3.accept(this, argu);
        if (!exp_3.IsSameClass(BaseTypeList.mint))
            ErrorHandler.TypeCheckError("new Arraytype should be with a int subscript.");

        return BaseTypeList.marraytype;
    }

    /**
     * f0 -> "new"
     * f1 -> Identifier()
     * f2 -> "("
     * f3 -> ")"
     */
    public VisitorAccepter visit(AllocationExpression n, SymbolSaver argu) {
        MClass mc = argu.GetSymbolTable().GetClass(n.f1.f0.tokenImage);
        if (mc == null)
            ErrorHandler.TypeCheckError(n.f1.f0.tokenImage + ": class not exists, could not new.");

        return mc;
    }

    /**
     * f0 -> "!"
     * f1 -> Expression()
     */
    public VisitorAccepter visit(NotExpression n, SymbolSaver argu) {
        MClass exp_1 = (MClass) n.f1.accept(this, argu);
        if (!exp_1.IsSameClass(BaseTypeList.mboolean))
            ErrorHandler.TypeCheckError("object under ! should be with boolean type.");

        return BaseTypeList.mboolean;
    }

    /**
     * f0 -> "("
     * f1 -> Expression()
     * f2 -> ")"
     */
    public VisitorAccepter visit(BracketExpression n, SymbolSaver argu) {
        return n.f1.accept(this, argu);
    }

}

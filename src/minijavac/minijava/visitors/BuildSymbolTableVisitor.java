package minijavac.minijava.visitors;

import minijavac.ErrorHandler;
import minijavac.minijava.JTBvisitor.*;
import minijavac.minijava.symbols.*;
import minijavac.minijava.syntaxtree.*;

/*
class IdentifierFetcher implements VisitorAccepter{
    MSymbolTable symbolTable;

    public enum fetchType {
            TYPE, VAR
    }
    fetchType type;

    IdentifierFetcher(MSymbolTable sTable, fetchType ft) {
        symbolTable = sTable;
        type = ft;
    }
}*/

public class BuildSymbolTableVisitor extends GJDepthFirst<VisitorAccepter, SymbolSaver> {

    /**
     * f0 -> "class"
     * f1 -> Identifier()
     * f2 -> "{"
     * f3 -> "public"
     * f4 -> "static"
     * f5 -> "void"
     * f6 -> "main"
     * f7 -> "("
     * f8 -> "String"
     * f9 -> "["
     * f10 -> "]"
     * f11 -> Identifier()
     * f12 -> ")"
     * f13 -> "{"
     * f14 -> ( VarDeclaration() )*
     * f15 -> ( Statement() )*
     * f16 -> "}"
     * f17 -> "}"
     */
    public VisitorAccepter visit(MainClass n, SymbolSaver argu) {
        MSymbolTable symbolTable = (MSymbolTable)argu;
        String className = n.f1.f0.tokenImage;
        String argName = n.f11.f0.tokenImage;

        MExtClass mc = new MExtClass(className, symbolTable);
        MMethod main = new MMethod(n.f6.tokenImage, mc, BaseTypeList.mvoid);
        main.AddParameter(new MVar(argName, BaseTypeList.mArgs), true);
        mc.AddMethod(main);
        symbolTable.AddClass(mc, true);

        //n.f14.accept(this, main);
        //n.f15.accept(this, main);

        return symbolTable;
    }


    /**
     * f0 -> "class"
     * f1 -> Identifier()
     * f2 -> "{"
     * f3 -> ( VarDeclaration() )*
     * f4 -> ( MethodDeclaration() )*
     * f5 -> "}"
     */
    public VisitorAccepter visit(ClassDeclaration n, SymbolSaver argu) {
        String className = n.f1.f0.tokenImage;
        MExtClass ec = (MExtClass)argu.lookup(className);


        n.f3.accept(this, ec);
        n.f4.accept(this, ec);

        return null;
    }

    /**
     * f0 -> "class"
     * f1 -> Identifier()
     * f2 -> "extends"
     * f3 -> Identifier()
     * f4 -> "{"
     * f5 -> ( VarDeclaration() )*
     * f6 -> ( MethodDeclaration() )*
     * f7 -> "}"
     */
    public VisitorAccepter visit(ClassExtendsDeclaration n, SymbolSaver argu) {
        MSymbolTable symbolTable = (MSymbolTable)argu;
        String className = n.f1.f0.tokenImage;
        MExtClass ec = (MExtClass)argu.lookup(className);


        // Inheritance
        String superClassName = n.f3.f0.tokenImage;
        MExtClass superClass = (MExtClass)symbolTable.GetClass(superClassName);
        if (superClass == null) {
            ErrorHandler.TypeCheckError(superClassName + ": class not exists, can not extend");
        }
        ec.Inherit(superClass);
        n.f5.accept(this, ec);
        n.f6.accept(this, ec);

        return null;
    }


    /**
     * f0 -> Type()
     * f1 -> Identifier()
     * f2 -> ";"
     */
    public VisitorAccepter visit(VarDeclaration n, SymbolSaver argu) {
        MClass type = (MClass)n.f0.accept(this, argu.GetSymbolTable());
        MVar var = new MVar(n.f1.f0.tokenImage, type);

        if (argu instanceof MExtClass) {
            MExtClass c = (MExtClass)argu;
            c.AddMemberVariable(var);
            return null;
        }
        argu.GetScope().AddVars(var, (MSymbol)argu);
        return null;
    }


    /**
     * f0 -> "public"
     * f1 -> Type()
     * f2 -> Identifier()
     * f3 -> "("
     * f4 -> ( FormalParameterList() )?
     * f5 -> ")"
     * f6 -> "{"
     * f7 -> ( VarDeclaration() )*
     * f8 -> ( Statement() )*
     * f9 -> "return"
     * f10 -> Expression()
     * f11 -> ";"
     * f12 -> "}"
     */
    public VisitorAccepter visit(MethodDeclaration n, SymbolSaver argu) {
        MExtClass ec = (MExtClass)argu;
        MClass type = (MClass)n.f1.accept(this, ec.GetSymbolTable());
        MMethod method = new MMethod(n.f2.f0.tokenImage, ec, type);

        n.f4.accept(this, method);

        ec.AddMethod(method);

        return null;
    }

    /**
     * f0 -> Type()
     * f1 -> Identifier()
     */
    public VisitorAccepter visit(FormalParameter n, SymbolSaver argu) {
        MMethod method = (MMethod)argu;
        MClass type = (MClass)n.f0.accept(this, method.GetSymbolTable());

        method.AddParameter(new MVar(n.f1.f0.tokenImage, type), true);
        return null;
    }

    /**
     * f0 -> ArrayType()
     *       | BooleanType()
     *       | IntegerType()
     *       | Identifier()
     */
    public VisitorAccepter visit(Type n, SymbolSaver argu) {
        return n.f0.accept(this, argu);
    }

    /**
     * f0 -> "int"
     * f1 -> "["
     * f2 -> "]"
     */
    public VisitorAccepter visit(ArrayType n, SymbolSaver argu) {
        return BaseTypeList.marraytype;
    }

    /**
     * f0 -> "boolean"
     */
    public VisitorAccepter visit(BooleanType n, SymbolSaver argu) {
        return BaseTypeList.mboolean;
    }

    /**
     * f0 -> "int"
     */
    public VisitorAccepter visit(IntegerType n, SymbolSaver argu) {
        return BaseTypeList.mint;
    }

    /**
     * f0 -> <IDENTIFIER>
     */
    public VisitorAccepter visit(Identifier n, SymbolSaver argu) {
        MSymbol symbol = argu.lookup(n.f0.tokenImage);
        if (symbol == null) {
            ErrorHandler.TypeCheckError(n.f0.tokenImage + ": symbol not exists in scope.");
        }
        return symbol;
    }

}

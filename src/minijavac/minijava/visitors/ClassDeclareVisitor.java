package minijavac.minijava.visitors;

import minijavac.ErrorHandler;
import minijavac.minijava.JTBvisitor.GJDepthFirst;
import minijavac.minijava.symbols.*;
import minijavac.minijava.syntaxtree.*;

public class ClassDeclareVisitor extends GJDepthFirst<VisitorAccepter, SymbolSaver> {
    /**
     * f0 -> "class"
     * f1 -> Identifier()
     * f2 -> "{"
     * f3 -> ( VarDeclaration() )*
     * f4 -> ( MethodDeclaration() )*
     * f5 -> "}"
     */
    public VisitorAccepter visit(ClassDeclaration n, SymbolSaver argu) {
        MSymbolTable symbolTable = (MSymbolTable)argu;
        String className = n.f1.f0.tokenImage;

        MExtClass ec = new MExtClass(className, symbolTable);
        symbolTable.AddClass(ec, false);

        return symbolTable;
    }

    /**
     * f0 -> "class"
     * f1 -> Identifier()
     * f2 -> "extends"
     * f3 -> Identifier()
     * f4 -> "{"
     * f5 -> ( VarDeclaration() )*
     * f6 -> ( MethodDeclaration() )*
     * f7 -> "}"
     */
    public VisitorAccepter visit(ClassExtendsDeclaration n, SymbolSaver argu) {
        MSymbolTable symbolTable = (MSymbolTable)argu;
        String className = n.f1.f0.tokenImage;

        /*
        // Inheritance is delayed to Symbol Building
        String superClassName = n.f3.f0.tokenImage;
        MExtClass superClass = (MExtClass)symbolTable.GetClass(superClassName);
        if (superClass == null) {
            ErrorHandler.TypeCheckError(superClassName + ": class not exists, can not extend");
        }*/

        MExtClass ec = new MExtClass(className, symbolTable);
        symbolTable.AddClass(ec, false);

        return symbolTable;
    }
}

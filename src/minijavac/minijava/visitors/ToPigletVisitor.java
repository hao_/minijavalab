package minijavac.minijava.visitors;

import javafx.util.Pair;
import minijavac.ErrorHandler;
import minijavac.Specification;
import minijavac.minijava.JTBvisitor.GJDepthFirst;
import minijavac.minijava.symbols.*;
import minijavac.minijava.syntaxtree.*;
import minijavac.piglet.PigletProgram;
import minijavac.piglet.exp.*;
import minijavac.piglet.exp.Error;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

// Simplified var
class Var {
    private MClass type;
    int tempUnit;

    Var(MClass _type) {
        type = _type;
        tempUnit = -1;
    }

    String getTypeName() {
        return type.name;
    }
}
// Simplified symbol table, only save vars, do no typecheck
class SSymbolTable {

    public HashMap<String, Var> symbols;

    SSymbolTable() {
        symbols = new HashMap<>();
    }

    SSymbolTable(SSymbolTable b) {
        symbols = new HashMap<>();
        for (Map.Entry<String, Var> s : b.symbols.entrySet()) {
            symbols.put(s.getKey(), s.getValue());
        }
    }
}

class PigletContext {
    Exp curExp;
    String curClass, curTypeDeclared;
    String retType;
    FuncsList accumulateFuncs;
    int curParaNum;
    boolean curGetType;
    PigletProgram pigletProgram;
    SSymbolTable ssymbolsTable; // context symbols, only contain local vars
    MSymbolTable msymbolTable; // the whole symbol table, contains class info

    PigletContext(PigletProgram _pigletProgram, MSymbolTable _msymbolTable) {
        curExp = null;
        curClass = "__NOCLASS__";
        curTypeDeclared = "";
        retType = "";
        curParaNum = 0;
        curGetType = false;
        pigletProgram = _pigletProgram;
        ssymbolsTable = new SSymbolTable();
        msymbolTable = _msymbolTable;
    }

    Var lookupVar(String name) {
        return ssymbolsTable.symbols.get(name);
    }

    void declareVar(String name, String typename) {
        MClass type = msymbolTable.GetClass(typename);
        if (type == null) {
            ErrorHandler.PigletError(String.format("declare a non-exists class %s", typename));
        }

        Var var;
        var = new Var(type);
        ssymbolsTable.symbols.put(name, var);
    }
    void bindTemp(String name, int tempUnit) {
        Var v = lookupVar(name);
        if (v == null) {
            ErrorHandler.PigletError("Init a non-exists var");
        }
        v.tempUnit = tempUnit;
    }
    MExtClass lookupExtClass(String name) {
        MExtClass ret = (MExtClass)msymbolTable.GetClass(name);
        if (ret == null) {
            ErrorHandler.PigletError(String.format("lookupExtClass() failed. Need %s", name));
        }
        return ret;
    }
}

public class ToPigletVisitor extends GJDepthFirst<Exp, PigletContext> {
    public static void VisitorEntry(Node root, PigletProgram pigletProgram, MSymbolTable msymbolTable) {
        root.accept(new ToPigletVisitor(), new PigletContext(
                pigletProgram,
                msymbolTable
        ));
    }
    /**
     * f0 -> MainClass()
     * f1 -> ( TypeDeclaration() )*
     * f2 -> <EOF>
     */
    public Exp visit(Goal n, PigletContext argu) {
        Program program = argu.pigletProgram.program;
        MainFunc mainFunc = (MainFunc)n.f0.accept(this, argu);
        if (mainFunc == null) {
            ErrorHandler.PigletError("DEBUG11");
        }
        program.SetMain(mainFunc);
        argu.curExp = program;
        n.f1.accept(this, argu);
        return null;
    }

    /**
     * f0 -> "class"
     * f1 -> Identifier()
     * f2 -> "{"
     * f3 -> "public"
     * f4 -> "static"
     * f5 -> "void"
     * f6 -> "main"
     * f7 -> "("
     * f8 -> "String"
     * f9 -> "["
     * f10 -> "]"
     * f11 -> Identifier()
     * f12 -> ")"
     * f13 -> "{"
     * f14 -> ( VarDeclaration() )*
     * f15 -> ( Statement() )*
     * f16 -> "}"
     * f17 -> "}"
     */
    public Exp visit(MainClass n, PigletContext argu) {
        /* Save and update ssymbols */
        SSymbolTable oldSSymbolTable = argu.ssymbolsTable;
        argu.ssymbolsTable = new SSymbolTable(argu.ssymbolsTable);
        argu.curClass = n.f1.f0.tokenImage;
        n.f14.accept(this, argu);


        StmtList stmtList = new StmtList();
        if (n.f15.present()) {
            for (Enumeration<Node> e = n.f15.elements(); e.hasMoreElements(); ) {
                stmtList.addStmtList((StmtList)e.nextElement().accept(this,argu));
            }
        }

        argu.ssymbolsTable = oldSSymbolTable;
        return new MainFunc(stmtList.toStmtArray());
    }

    /**
     * f0 -> ClassDeclaration()
     *       | ClassExtendsDeclaration()
     */
    public Exp visit(TypeDeclaration n, PigletContext argu) {
        Program program = (Program)argu.curExp;
        FuncsList funcsList = (FuncsList)n.f0.accept(this, argu);
        program.MergeFuncList(funcsList);
        argu.curExp = program;
        return null;
    }

    /**
     * f0 -> "class"
     * f1 -> Identifier()
     * f2 -> "{"
     * f3 -> ( VarDeclaration() )*
     * f4 -> ( MethodDeclaration() )*
     * f5 -> "}"
     */
    public Exp visit(ClassDeclaration n, PigletContext argu) {
        argu.curClass = n.f1.f0.tokenImage;
        argu.accumulateFuncs = new FuncsList();
        n.f4.accept(this, argu);
        return argu.accumulateFuncs;
    }

    /**
     * f0 -> "class"
     * f1 -> Identifier()
     * f2 -> "extends"
     * f3 -> Identifier()
     * f4 -> "{"
     * f5 -> ( VarDeclaration() )*
     * f6 -> ( MethodDeclaration() )*
     * f7 -> "}"
     */
    public Exp visit(ClassExtendsDeclaration n, PigletContext argu) {
        argu.curClass = n.f1.f0.tokenImage;
        argu.accumulateFuncs = new FuncsList();
        n.f6.accept(this, argu);
        return argu.accumulateFuncs;
    }

    /**
     * f0 -> Type()
     * f1 -> Identifier()
     * f2 -> ";"
     */
    public Exp visit(VarDeclaration n, PigletContext argu) {
        n.f0.accept(this, argu);
        argu.declareVar(n.f1.f0.tokenImage, argu.curTypeDeclared);
        int tempUnit = argu.pigletProgram.tempUnitPool.New();
        argu.bindTemp(n.f1.f0.tokenImage, tempUnit);
        return null;
    }

    /**
     * f0 -> "public"
     * f1 -> Type()
     * f2 -> Identifier()
     * f3 -> "("
     * f4 -> ( FormalParameterList() )?
     * f5 -> ")"
     * f6 -> "{"
     * f7 -> ( VarDeclaration() )*
     * f8 -> ( Statement() )*
     * f9 -> "return"
     * f10 -> Expression()
     * f11 -> ";"
     * f12 -> "}"
     */
    public Exp visit(MethodDeclaration n, PigletContext argu) {
        String funcName = Specification.PigletFuncName(argu.curClass, n.f2.f0.tokenImage);

        // Saved context
        SSymbolTable oldSSymbolTable = argu.ssymbolsTable;
        argu.ssymbolsTable = new SSymbolTable(argu.ssymbolsTable);

        // Bind parameter to temp unit
        argu.curParaNum = 1;
        n.f4.accept(this, argu);
        // Bind "this" to the temp 0
        argu.declareVar("this", argu.curClass);
        argu.bindTemp("this", 0);
        n.f7.accept(this, argu);

        int paraNum = argu.curParaNum;

        StmtList stmtList = new StmtList();
        if (n.f8.present()) {
            for (Enumeration<Node> e = n.f8.elements(); e.hasMoreElements(); ) {
                stmtList.addStmtList((StmtList)e.nextElement().accept(this,argu));
            }
        }

        Exp retExp = n.f10.accept(this, argu);

        argu.accumulateFuncs.AddFunc(
                new Func(funcName, paraNum, stmtList.toStmtArray(), retExp)
        );

        argu.ssymbolsTable = oldSSymbolTable;
        return null;
    }

    /**
     * f0 -> FormalParameter()
     * f1 -> ( FormalParameterRest() )*
     */
    public Exp visit(FormalParameterList n, PigletContext argu) {
        n.f0.accept(this, argu);
        n.f1.accept(this, argu);
        return null;
    }

    /**
     * f0 -> Type()
     * f1 -> Identifier()
     */
    public Exp visit(FormalParameter n, PigletContext argu) {
        String identifier = n.f1.f0.tokenImage;
        n.f0.accept(this, argu);
        argu.declareVar(identifier, argu.curTypeDeclared);
        argu.bindTemp(identifier, argu.curParaNum);
        argu.curParaNum++;
        return null;
    }

    /**
     * f0 -> ","
     * f1 -> FormalParameter()
     */
    public Exp visit(FormalParameterRest n, PigletContext argu) {
        n.f1.accept(this, argu);
        return null;
    }

    /**
     * f0 -> ArrayType()
     *       | BooleanType()
     *       | IntegerType()
     *       | Identifier()
     */
    public Exp visit(Type n, PigletContext argu) {
        argu.curGetType = true;
        n.f0.accept(this, argu);
        argu.curGetType = false;
        return null;
    }

    /**
     * f0 -> "int"
     * f1 -> "["
     * f2 -> "]"
     */
    public Exp visit(ArrayType n, PigletContext argu) {
        argu.curTypeDeclared = "int[]";
        return null;
    }

    /**
     * f0 -> "boolean"
     */
    public Exp visit(BooleanType n, PigletContext argu) {
        argu.curTypeDeclared = "boolean";
        return null;
    }

    /**
     * f0 -> "int"
     */
    public Exp visit(IntegerType n, PigletContext argu) {
        argu.curTypeDeclared = "int";
        return null;
    }

    /**
     * f0 -> Block()
     *       | AssignmentStatement()
     *       | ArrayAssignmentStatement()
     *       | IfStatement()
     *       | WhileStatement()
     *       | PrintStatement()
     */
    public Exp visit(Statement n, PigletContext argu) {
        StmtList ret = (StmtList)n.f0.accept(this, argu);
        if (ret.stmts.size() == 0) {
            ret.addStmt(new Noop());
        }
        return ret;
    }

    /**
     * f0 -> "{"
     * f1 -> ( Statement() )*
     * f2 -> "}"
     */
    public Exp visit(Block n, PigletContext argu) {
        StmtList stmtList = new StmtList();
        if (n.f1.present()) {
            for (Enumeration<Node> e = n.f1.elements(); e.hasMoreElements(); ) {
                stmtList.addStmtList((StmtList)e.nextElement().accept(this,argu));
            }
        }
        return stmtList;
    }

    /**
     * f0 -> Identifier()
     * f1 -> "="
     * f2 -> Expression()
     * f3 -> ";"
     */
    public Exp visit(AssignmentStatement n, PigletContext argu) {
        String identifier = n.f0.f0.tokenImage;

        Exp exp = n.f2.accept(this, argu);

        Var var = argu.lookupVar(identifier);
        if (var != null) {
            return new StmtList(new Move(var.tempUnit, exp));
        }

        MExtClass nowClass = argu.lookupExtClass(argu.curClass);
        int off = nowClass.layoutFieldOff(identifier);
        return new StmtList(
                new Hstore(new Temp(0), off, exp)
        );
    }

    /**
     * f0 -> Identifier()
     * f1 -> "["
     * f2 -> Expression()
     * f3 -> "]"
     * f4 -> "="
     * f5 -> Expression()
     * f6 -> ";"
     */
    public Exp visit(ArrayAssignmentStatement n, PigletContext argu) {
        int array = argu.pigletProgram.tempUnitPool.New();
        int key = argu.pigletProgram.tempUnitPool.New();
        int len = argu.pigletProgram.tempUnitPool.New();

        String identifier = n.f0.f0.tokenImage;

        Exp keyexp = n.f2.accept(this, argu);
        Exp resexp = n.f5.accept(this, argu);
        Exp id;

        Var var = argu.lookupVar(identifier);
        if (var != null) {
            id = new Temp(var.tempUnit);
        } else {
            MExtClass nowClass = argu.lookupExtClass(argu.curClass);
            int off = nowClass.layoutFieldOff(identifier);

            int tmp = argu.pigletProgram.tempUnitPool.New();
            id = new BeginBlock(
                    new Stmt[]{
                            new Hload(tmp, new Temp(0), off)
                    },
                    new Temp(tmp)
            );
            argu.pigletProgram.tempUnitPool.Recycle(tmp);
        }

        String labelErr = argu.pigletProgram.labelPool.New();
        String labelFin = argu.pigletProgram.labelPool.New();

        Stmt err = new Error();
        err.SetLabel(labelErr);
        Stmt assign = new Hstore(ExpMacro.ArrayKey(array, key), 0, resexp);
        assign.SetLabel(labelFin);

        argu.pigletProgram.tempUnitPool.Recycle(array, key, len);

        return new StmtList(
                    new Move(array, id),
                    new Move(key, keyexp),
                    new Hload(len, new Temp(array), 0),
                    new CJump(new Lt(new Temp(key), new Temp(len)), labelErr),
                    new CJump(new Lt(new Minus(new Num(0), new Num(1)), new Temp(key)), labelErr),
                    new Jump(labelFin),
                    err,
                    assign
        );
    }

    /**
     * f0 -> "if"
     * f1 -> "("
     * f2 -> Expression()
     * f3 -> ")"
     * f4 -> Statement()
     * f5 -> "else"
     * f6 -> Statement()
     */
    public Exp visit(IfStatement n, PigletContext argu) {
        String labelfalse = argu.pigletProgram.labelPool.New();
        String labelfin = argu.pigletProgram.labelPool.New();

        Exp exp1 = n.f2.accept(this, argu);
        StmtList trueTarget = (StmtList)n.f4.accept(this, argu);
        StmtList falseTarget = (StmtList)n.f6.accept(this, argu);
        falseTarget.stmts.get(0).SetLabel(labelfalse);
        Stmt expfin = new Noop();
        expfin.SetLabel(labelfin);
        return new StmtList(
                new CJump(exp1, labelfalse),
                trueTarget,
                new Jump(labelfin),
                falseTarget,
                expfin
        );
    }

    /**
     * f0 -> "while"
     * f1 -> "("
     * f2 -> Expression()
     * f3 -> ")"
     * f4 -> Statement()
     */
    public Exp visit(WhileStatement n, PigletContext argu) {
        String labelfin = argu.pigletProgram.labelPool.New();
        String labelstart = argu.pigletProgram.labelPool.New();

        Exp exp = n.f2.accept(this, argu);
        Stmt judge = new CJump(exp, labelfin);
        judge.SetLabel(labelstart);
        Stmt fin = new Noop();
        fin.SetLabel(labelfin);

        StmtList cirStmt = (StmtList) n.f4.accept(this, argu);

        return new StmtList(
                judge,
                cirStmt,
                new Jump(labelstart),
                fin
        );
    }

    /**
     * f0 -> "System.out.println"
     * f1 -> "("
     * f2 -> Expression()
     * f3 -> ")"
     * f4 -> ";"
     */
    public Exp visit(PrintStatement n, PigletContext argu) {
        Stmt content = (Stmt)n.f2.accept(this, argu);
        return new StmtList(new PrintInstr(content));
    }

    /**
     * f0 -> AndExpression()
     *       | CompareExpression()
     *       | PlusExpression()
     *       | MinusExpression()
     *       | TimesExpression()
     *       | ArrayLookup()
     *       | ArrayLength()
     *       | MessageSend()
     *       | PrimaryExpression()
     */
    public Exp visit(Expression n, PigletContext argu) {
        return n.f0.accept(this, argu);
    }

    /**
     * f0 -> PrimaryExpression()
     * f1 -> "&&"
     * f2 -> PrimaryExpression()
     */
    public Exp visit(AndExpression n, PigletContext argu) {
        int res = argu.pigletProgram.tempUnitPool.New();
        String trueLabel = argu.pigletProgram.labelPool.New();
        String finLabel = argu.pigletProgram.labelPool.New();


        Exp exp1 = n.f0.accept(this, argu);
        Exp exp2 = n.f2.accept(this, argu);
        Stmt fin = new Noop();
        fin.SetLabel(finLabel);
        Stmt setTrue = new Move(res, new Num(1));
        setTrue.SetLabel(trueLabel);

        argu.pigletProgram.tempUnitPool.Recycle(res);

        return new BeginBlock(
                new Stmt[]{
                        new Move(res, new Plus(exp1, exp2)),
                        new CJump(new Lt(new Temp(res), new Num(2)), trueLabel),
                        new Move(res, new Num(0)),
                        new Jump(finLabel),
                        setTrue,
                        fin
                },
                new Temp(res)
        );
    }

    /**
     * f0 -> PrimaryExpression()
     * f1 -> "<"
     * f2 -> PrimaryExpression()
     */
    public Exp visit(CompareExpression n, PigletContext argu) {
        return new Lt(n.f0.accept(this, argu), n.f2.accept(this, argu));
    }

    /**
     * f0 -> PrimaryExpression()
     * f1 -> "+"
     * f2 -> PrimaryExpression()
     */
    public Exp visit(PlusExpression n, PigletContext argu) {
        return new Plus(n.f0.accept(this, argu), n.f2.accept(this, argu));
    }

    /**
     * f0 -> PrimaryExpression()
     * f1 -> "-"
     * f2 -> PrimaryExpression()
     */
    public Exp visit(MinusExpression n, PigletContext argu) {
        return new Minus(n.f0.accept(this, argu), n.f2.accept(this, argu));
    }

    /**
     * f0 -> PrimaryExpression()
     * f1 -> "*"
     * f2 -> PrimaryExpression()
     */
    public Exp visit(TimesExpression n, PigletContext argu) {
        return new Times(n.f0.accept(this, argu), n.f2.accept(this, argu));
    }

    /**
     * f0 -> PrimaryExpression()
     * f1 -> "["
     * f2 -> PrimaryExpression()
     * f3 -> "]"
     */
    public Exp visit(ArrayLookup n, PigletContext argu) {
        int array = argu.pigletProgram.tempUnitPool.New();
        int key = argu.pigletProgram.tempUnitPool.New();
        int len = argu.pigletProgram.tempUnitPool.New();

        String labelErr = argu.pigletProgram.labelPool.New();
        String labelFin = argu.pigletProgram.labelPool.New();

        Stmt err = new Error();
        err.SetLabel(labelErr);
        Stmt assign = new Hload(array, ExpMacro.ArrayKey(array, key), 0);
        assign.SetLabel(labelFin);


        Exp exp1 = n.f0.accept(this, argu);
        Exp exp2 = n.f2.accept(this, argu);

        argu.pigletProgram.tempUnitPool.Recycle(array, key, len);

        argu.retType = "int";

        return new BeginBlock(
                new Stmt[]{
                        new Move(array, exp1),
                        new Move(key, exp2),
                        new Hload(len, new Temp(array), 0),
                        new CJump(new Lt(new Temp(key), new Temp(len)), labelErr),
                        new CJump(new Lt(new Minus(new Num(0), new Num(1)), new Temp(key)), labelErr),
                        new Jump(labelFin),
                        err,
                        assign
                },
                new Temp(array)
        );
    }

    /**
     * f0 -> PrimaryExpression()
     * f1 -> "."
     * f2 -> "length"
     */
    public Exp visit(ArrayLength n, PigletContext argu) {
        int res = argu.pigletProgram.tempUnitPool.New();
        argu.pigletProgram.tempUnitPool.Recycle(res);
        argu.retType = "int";
        return new BeginBlock(
                new Stmt[]{
                        new Hload(res, n.f0.accept(this, argu), 0)
                },
                new Temp(res)
        );
    }

    /**
     * f0 -> PrimaryExpression()
     * f1 -> "."
     * f2 -> Identifier()
     * f3 -> "("
     * f4 -> ( ExpressionList() )?
     * f5 -> ")"
     */
    public Exp visit(MessageSend n, PigletContext argu) {
        int obj = argu.pigletProgram.tempUnitPool.New();
        int tmp = argu.pigletProgram.tempUnitPool.New();
        int func = argu.pigletProgram.tempUnitPool.New();

        Exp exp0 = n.f0.accept(this, argu);
        MExtClass objClass = argu.lookupExtClass(argu.retType);
        int methodOff = objClass.layoutMethodOff(n.f2.f0.tokenImage).getValue();

        StmtList exp4 = (StmtList) n.f4.accept(this, argu);
        if (exp4 == null) {
            // Empty parameter list
            exp4 = new StmtList();
        }


        argu.pigletProgram.tempUnitPool.Recycle(obj, func, tmp);

        return new BeginBlock(
                (new StmtList(
                        new Move(obj, exp0),
                        new Hload(tmp, new Temp(obj), MExtClass.layoutMethodTableOff), // get method table
                        new Hload(func, new Temp(tmp), methodOff)
                )).toStmtArray(),
                new Call(new Temp(func), (new StmtList(new Temp(obj), exp4)).toStmtArray())
        );
    }

    /**
     * f0 -> Expression()
     * f1 -> ( ExpressionRest() )*
     */
    public Exp visit(ExpressionList n, PigletContext argu) {
        Exp oldCur = argu.curExp;
        argu.curExp = new StmtList(n.f0.accept(this, argu));
        n.f1.accept(this, argu);
        Exp tmp = argu.curExp;
        argu.curExp = oldCur;
        return tmp;
    }

    /**
     * f0 -> ","
     * f1 -> Expression()
     */
    public Exp visit(ExpressionRest n, PigletContext argu) {
        ((StmtList)argu.curExp).addStmt((Stmt)n.f1.accept(this, argu));
        return null;
    }

    /**
     * f0 -> IntegerLiteral()
     *       | TrueLiteral()
     *       | FalseLiteral()
     *       | Identifier()
     *       | ThisExpression()
     *       | ArrayAllocationExpression()
     *       | AllocationExpression()
     *       | NotExpression()
     *       | BracketExpression()
     */
    public Exp visit(PrimaryExpression n, PigletContext argu) {
        return n.f0.accept(this, argu);
    }

    /**
     * f0 -> <INTEGER_LITERAL>
     */
    public Exp visit(IntegerLiteral n, PigletContext argu) {
        argu.retType = "int";
        return new Num(Integer.valueOf(n.f0.tokenImage));
    }

    /**
     * f0 -> "true"
     */
    public Exp visit(TrueLiteral n, PigletContext argu) {
        argu.retType = "boolean";
        return new Num(1);
    }

    /**
     * f0 -> "false"
     */
    public Exp visit(FalseLiteral n, PigletContext argu) {
        argu.retType = "boolean";
        return new Num(0);
    }

    /**
     * f0 -> <IDENTIFIER>
     */
    public Exp visit(Identifier n, PigletContext argu) {
        if (argu.curGetType) {
            argu.curGetType = false;
            argu.curTypeDeclared = n.f0.tokenImage;
            return new Error();
        }
        String identifier = n.f0.tokenImage;


        Var var = argu.lookupVar(identifier);
        if (var != null) {
            argu.retType = var.getTypeName();
            return new Temp(var.tempUnit);
        }

        int res = argu.pigletProgram.tempUnitPool.New();
        argu.pigletProgram.tempUnitPool.Recycle(res);

        MExtClass nowClass = argu.lookupExtClass(argu.curClass);
        int off = nowClass.layoutFieldOff(identifier);
        return new BeginBlock(
                new Stmt[]{
                        new Hload(res, new Temp(0), off)
                },
                new Temp(res)
        );
    }

    /**
     * f0 -> "this"
     */
    public Exp visit(ThisExpression n, PigletContext argu) {
        argu.retType = argu.curClass;
        return new Temp(0);
    }

    /**
     * f0 -> "new"
     * f1 -> "int"
     * f2 -> "["
     * f3 -> Expression()
     * f4 -> "]"
     */
    public Exp visit(ArrayAllocationExpression n, PigletContext argu) {
        int len = argu.pigletProgram.tempUnitPool.New();
        int array = argu.pigletProgram.tempUnitPool.New();

        String errLabel = argu.pigletProgram.labelPool.New();
        String okLabel = argu.pigletProgram.labelPool.New();

        Exp exp3 = n.f3.accept(this, argu);
        Stmt err = new Error();
        err.SetLabel(errLabel);
        Stmt newArray = new Move(array,
                new Hallocate(new Plus(new Num(4), new Times(new Num(4), new Temp(len))))
        );
        newArray.SetLabel(okLabel);

        argu.retType = "int[]";
        argu.pigletProgram.tempUnitPool.Recycle(len, array);

        return new BeginBlock(
                new Stmt[]{
                        new Move(len, exp3),
                        new CJump( new Lt(new Minus(new Num(0), new Num(1)), new Temp(len)), errLabel ),
                        new Jump(okLabel),
                        err,
                        newArray,
                        new Hstore(new Temp(array), 0, new Temp(len))
                },
                new Temp(array)
        );
    }

    /**
     * f0 -> "new"
     * f1 -> Identifier()
     * f2 -> "("
     * f3 -> ")"
     */
    public Exp visit(AllocationExpression n, PigletContext argu) {
        String identifier = n.f1.f0.tokenImage;
        argu.retType = identifier;
        MExtClass nowClass = argu.lookupExtClass(identifier);

        int obj = argu.pigletProgram.tempUnitPool.New();
        int methodTable = argu.pigletProgram.tempUnitPool.New();
        StmtList stmtList = new StmtList();
        // instance table
        stmtList.addStmt(new Move(obj, new Hallocate(new Num(nowClass.layoutSize()))));

        // method table
        stmtList.addStmt(new Move(methodTable, new Hallocate(new Num(nowClass.MethodTableSize()))));
        stmtList.addStmt(new Hstore(new Temp(obj), MExtClass.layoutMethodTableOff, new Temp(methodTable)));
        for (Map.Entry<String, Pair<String, Integer>> entry : nowClass.AllMethodOff().entrySet()) {
            String funcName = entry.getValue().getKey();
            int off = entry.getValue().getValue();
            stmtList.addStmt(new Hstore(new Temp(methodTable), off,
                    new Symbol(funcName)));
        }

        argu.pigletProgram.tempUnitPool.Recycle(obj, methodTable);

        return new BeginBlock(
                stmtList.toStmtArray(),
                new Temp(obj)
        );
    }

    /**
     * f0 -> "!"
     * f1 -> Expression()
     */
    public Exp visit(NotExpression n, PigletContext argu) {
        return new Minus(new Num(1), n.f1.accept(this, argu));
    }

    /**
     * f0 -> "("
     * f1 -> Expression()
     * f2 -> ")"
     */
    public Exp visit(BracketExpression n, PigletContext argu) {
        return n.f1.accept(this, argu);
    }
}

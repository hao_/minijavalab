package minijavac.mips;

import minijavac.Specification;
import minijavac.kanga.parser.KangaParser;
import minijavac.kanga.parser.Node;
import minijavac.kanga.parser.ParseException;
import minijavac.kanga.visitors.ToMIPSVisitor;

import java.io.PrintStream;
import java.util.Vector;

public class MIPSProgram {
    private Vector<String> proc;

    public MIPSProgram() {
        proc = new Vector<>();
    }

    public void FromKanga(KangaParser parser) {
        try {
            Node goal = parser.Goal();
            ToMIPSVisitor.VisitorEntry(goal, this);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void addLine(String line) {
        proc.addElement(line);
    }

    public void Print(PrintStream out) {
        for (String line: proc) {
            out.println(line);
        }
    }
}

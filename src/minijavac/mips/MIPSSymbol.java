package minijavac.mips;

import minijavac.ErrorHandler;

public class MIPSSymbol {
    public final static String Func_error = "__error_terminate";
    public final static String Func_halloc = "__hallocate";
    public final static String Func_print = "__print";


    private final static int Syscall_error = 10;
    private final static int Syscall_printString = 4;
    private final static int Syscall_printInt = 1;

    private final static String ErrorSymbol = "_str_err";
    private final static String NewLineSymbol = "_newline";
    private final static String ErrorInfo = "Error: termination";

    public final static String PredefinedCodes[] = {
            /* Func error */
            "        .text",
            String.format("        .globl %s", Func_error),
            String.format("%s:", Func_error),
            String.format("        li $v0, %d", Syscall_printString),
            String.format("        la $a0, %s", ErrorSymbol),
            "        syscall",
            String.format("        li $v0, %d", MIPSSymbol.Syscall_error),
            "        syscall",
            "",
            /* Func print */
            "        .text",
            String.format("        .globl %s", Func_print),
            String.format("%s:", Func_print),
            String.format("        li $v0, %d", Syscall_printInt),
            "        syscall",
            String.format("        la $a0, %s", NewLineSymbol),
            String.format("        li $v0, %d", Syscall_printString),
            "        syscall",
            "        j $ra",
            "",
            /* Func alloc */
            "        .text",
            String.format("        .globl %s", Func_halloc),
            String.format("%s:", Func_halloc),
            "        li $v0, 9",
            "        syscall",
            "        j $ra",
            "",
            /* Data error info */
            "        .data",
            "        .align  0",
            String.format("%s:  .asciiz \"%s\"", ErrorSymbol,  ErrorInfo),
            "",
            /* Data new line */
            "        .data",
            "        .align  0",
            String.format("%s:  .asciiz \"\\n\"", NewLineSymbol),
            ""

    };


    public static String RegFromKanga(String reg) {
        if (reg.equals("")) {
            ErrorHandler.MIPSError("failed to translate Kanga register, empty register.");
        }
        char h = reg.charAt(0);
        if (h == 'a' || h == 'v' || h == 's' || h == 't') {
            return String.format("$%s", reg);
        }
        ErrorHandler.MIPSError(String.format("failed to translate Kanga register %s.",  reg));
        return "";
    }
}

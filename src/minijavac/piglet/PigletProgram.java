package minijavac.piglet;

import minijavac.minijava.symbols.MSymbolTable;
import minijavac.minijava.syntaxtree.Node;
import minijavac.minijava.visitors.ToPigletVisitor;
import minijavac.piglet.exp.Program;
import minijavac.piglet.resource.LabelPool;
import minijavac.piglet.resource.TempUnitPool;

import java.io.PrintStream;

public class PigletProgram {
    public LabelPool labelPool;
    public TempUnitPool tempUnitPool;
    public Program program;

    public PigletProgram() {
        labelPool = new LabelPool();
        tempUnitPool = new TempUnitPool();
        program = new Program();
    }

    public void print(PrintStream out) {
        program.print(out, 0);
    }

    public void FromMiniJava(Node astRoot, MSymbolTable symbolTable) {
        symbolTable.InitClassLayout();
        ToPigletVisitor.VisitorEntry(astRoot, this, symbolTable);
    }
}

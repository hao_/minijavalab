package minijavac.piglet.exp;

import minijavac.util.PrintHelper;

import java.io.PrintStream;

public class BeginBlock extends Stmt {
    public Stmt[] stmtList;
    public Exp retExp;

    public BeginBlock(Stmt[] _stmtList, Exp _retExp) {
        stmtList = _stmtList;
        retExp = _retExp;
    }

    @Override
    public void print(PrintStream out, int depth) {
        out.println("BEGIN");
        depth += 1;
        for (int i = 0; i < stmtList.length; i++) {
            PrintHelper.PigletIndentPrint(out, depth, stmtList[i]);
            stmtList[i].print(out, depth);
            out.println();
        }
        PrintHelper.PigletIndentPrint(out, depth, retExp);
        out.print("RETURN ");
        retExp.print(out, depth);
        out.println();
        PrintHelper.PigletIndentPrint(out, depth - 1, null);
        out.print("END");
    }
}

package minijavac.piglet.exp;

import java.io.PrintStream;

// CJump Exp Label
public class CJump extends Stmt {
    public Exp exp;
    public String label;

    public CJump(Exp _exp, String _label) {
        exp = _exp;
        label = _label;
    }

    @Override
    public void print(PrintStream out, int depth) {
        out.print("CJUMP ");
        exp.print(out, depth);
        out.printf(" %s", label);
    }
}

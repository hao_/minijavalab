package minijavac.piglet.exp;

import java.io.PrintStream;

// CALL Exp1 “(“ ( EXP# )* “)”
public class Call extends Stmt {
    public Exp bodyExp;
    public Exp[] argExps;

    public Call(Exp _bodyExp, Exp[] _argExps) {
        bodyExp = _bodyExp;
        argExps = _argExps;
    }

    @Override
    public void print(PrintStream out, int depth) {
        out.print("CALL ");
        bodyExp.print(out, depth);
        out.print(" (");
        for (int i = 0; i < argExps.length; i++) {
            out.print(" ");
            argExps[i].print(out, depth);
            out.print(" ");
        }
        out.print(")");
    }
}

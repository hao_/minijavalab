package minijavac.piglet.exp;

import java.io.PrintStream;

public class Error extends Stmt {
    public Error() {
    }

    @Override
    public void print(PrintStream out, int depth) {
        out.print("ERROR");
    }
}

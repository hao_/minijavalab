package minijavac.piglet.exp;

import java.io.PrintStream;

public abstract class Exp {
    public abstract void print(PrintStream out, int depth);
}

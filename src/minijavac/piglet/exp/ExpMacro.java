package minijavac.piglet.exp;

public class ExpMacro {
    // Temp k = Temp k * 4 + Temp array
    public static Stmt ArrayKey(int array, int key) {
        return new Plus(new Num(4), new Plus(new Temp(array), new Times(new Temp(key), new Num(4))));
    }
}

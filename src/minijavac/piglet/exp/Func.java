package minijavac.piglet.exp;

import minijavac.util.PrintHelper;

import java.io.PrintStream;

// Label“[”IntegerLiteral“]”"BEGIN"StmtList"RETURN"Exp"END"
public class Func extends Exp {
    public String funcName;
    public int argNum;
    public Stmt[] stmtList;
    public Exp retExp;

    public Func(String _funcName, int _argNum, Stmt[] _stmtList, Exp _retExp) {
        funcName = _funcName;
        argNum = _argNum;
        stmtList = _stmtList;
        retExp = _retExp;
    }

    @Override
    public void print(PrintStream out, int depth) {
        PrintHelper.PigletIndentPrint(out, depth, null);
        out.printf("%s [%d] BEGIN\n", funcName, argNum);
        depth += 1;
        for (int i = 0; i < stmtList.length; i++) {
            PrintHelper.PigletIndentPrint(out, depth, stmtList[i]);
            stmtList[i].print(out, depth);
            out.println();
        }
        PrintHelper.PigletIndentPrint(out, depth, retExp);
        out.print("RETURN ");
        retExp.print(out, depth);
        out.println();
        PrintHelper.PigletIndentPrint(out, depth - 1, null);
        out.println("END");
    }
}

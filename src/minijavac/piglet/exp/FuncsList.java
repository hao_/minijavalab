package minijavac.piglet.exp;

import minijavac.ErrorHandler;

import java.io.PrintStream;
import java.util.Vector;

public class FuncsList extends Exp {
    public Vector<Func> funcs;

    public FuncsList() {
        funcs = new Vector<Func>();
    }

    public void AddFunc(Func func) {
        funcs.addElement(func);
    }
    public void MergeFuncList(FuncsList funcsList) {
        for (int i = 0, sz = funcsList.funcs.size(); i < sz; i++) {
            funcs.addElement(funcsList.funcs.get(i));
        }
    }

    @Override
    public void print(PrintStream out, int depth) {
        ErrorHandler.PigletError("should never call FuncsList.print()");
    }
}

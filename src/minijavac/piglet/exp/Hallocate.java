package minijavac.piglet.exp;

import java.io.PrintStream;

// HALLOCATE Exp
public class Hallocate extends Stmt {
    public Exp exp;

    public Hallocate(Exp _exp) {
        exp = _exp;
    }

    @Override
    public void print(PrintStream out, int depth) {
        out.print("HALLOCATE ");
        exp.print(out, depth);
    }
}

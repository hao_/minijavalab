package minijavac.piglet.exp;

import java.io.PrintStream;

// HLOAD TEMP * Exp IntegerLiteral
public class Hload extends Stmt {
    public int tempUnit, off;
    public Exp exp;

    public Hload(int _tempUnit, Exp _exp, int _off) {
        tempUnit = _tempUnit;
        exp = _exp;
        off = _off;
    }

    @Override
    public void print(PrintStream out, int depth) {
        out.printf("HLOAD TEMP %d ", tempUnit);
        exp.print(out, depth);
        out.printf(" %d", off);
    }
}

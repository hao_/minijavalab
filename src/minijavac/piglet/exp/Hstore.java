package minijavac.piglet.exp;

import java.io.PrintStream;

// HSTORE Exp1 IntegerLiteral Exp2
public class Hstore extends Stmt {
    public Exp exp1, exp2;
    public int off;

    public Hstore(Exp _exp1, int _off, Exp _exp2) {
        exp1 = _exp1;
        off = _off;
        exp2 = _exp2;
    }

    @Override
    public void print(PrintStream out, int depth) {
        out.print("HSTORE ");
        exp1.print(out, depth);
        out.printf(" %d ", off);
        exp2.print(out, depth);
    }
}

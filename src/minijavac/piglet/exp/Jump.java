package minijavac.piglet.exp;

import java.io.PrintStream;

// JUMP Label
public class Jump extends Stmt {
    public String label;

    public Jump(String _label) {
        label = _label;
    }

    @Override
    public void print(PrintStream out, int depth) {
        out.printf("JUMP %s", label);
    }
}

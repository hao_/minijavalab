package minijavac.piglet.exp;

import java.io.PrintStream;

// LT Exp1 Exp2
// return 1 for Exp1 < Exp2
public class Lt extends Stmt {
    public Exp exp1, exp2;

    public Lt(Exp _exp1, Exp _exp2) {
        exp1 = _exp1;
        exp2 = _exp2;
    }

    @Override
    public void print(PrintStream out, int depth) {
        out.print("LT ");
        exp1.print(out, depth);
        out.print(" ");
        exp2.print(out, depth);
    }
}

package minijavac.piglet.exp;

import minijavac.util.PrintHelper;

import java.io.PrintStream;

public class MainFunc extends Exp {
    public Stmt[] stmtList;

    public MainFunc(Stmt[] stmts) {
        stmtList = new Stmt[stmts.length];
        for (int i = 0; i < stmtList.length; i++) {
            stmtList[i] = stmts[i];
        }
    }

    @Override
    public void print(PrintStream out, int depth) {
        out.println("MAIN");
        depth = 1;
        for (int i = 0; i < stmtList.length; i++) {
            PrintHelper.PigletIndentPrint(out, depth, stmtList[i]);
            stmtList[i].print(out, depth);
            out.println();
        }
        out.println("END");
    }
}

package minijavac.piglet.exp;

import java.io.PrintStream;

// MOVE TEMP [tempUnit] Exp
public class Move extends Stmt {
    public int tempUnit;
    public Exp exp;

    public Move(int _tempUnit, Exp _exp) {
        tempUnit = _tempUnit;
        exp = _exp;
    }

    @Override
    public void print(PrintStream out, int depth) {
        out.printf("MOVE TEMP %d ", tempUnit);
        exp.print(out, depth);
    }
}

package minijavac.piglet.exp;

import java.io.PrintStream;

public class Noop extends Stmt {
    public Noop() {}

    @Override
    public void print(PrintStream out, int depth) {
        out.print("NOOP");
    }
}

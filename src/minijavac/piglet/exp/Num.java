package minijavac.piglet.exp;

import java.io.PrintStream;

public class Num extends Stmt {
    public int num;

    public Num(int _num) {
        num = _num;
    }

    @Override
    public void print(PrintStream out, int depth) {
        out.printf("%d ", num);
    }
}

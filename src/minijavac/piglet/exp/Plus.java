package minijavac.piglet.exp;

import java.io.PrintStream;

// PLUS Exp1 Exp2
public class Plus extends Stmt {
    public Exp exp1, exp2;

    public Plus(Exp _exp1, Exp _exp2) {
        exp1 = _exp1;
        exp2 = _exp2;
    }

    @Override
    public void print(PrintStream out, int depth) {
        out.print("PLUS ");
        exp1.print(out, depth);
        out.print(" ");
        exp2.print(out, depth);
    }
}

package minijavac.piglet.exp;

import java.io.PrintStream;

// PRINT Exp
public class PrintInstr extends Stmt {
    public Exp exp;

    public PrintInstr(Exp _exp) {
        exp = _exp;
    }

    @Override
    public void print(PrintStream out, int depth) {
        out.print("PRINT ");
        exp.print(out, depth);
    }
}

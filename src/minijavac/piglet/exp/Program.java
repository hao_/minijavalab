package minijavac.piglet.exp;

import java.io.PrintStream;
import java.util.Vector;

public class Program extends Exp {
    public Exp mainFunc;
    public Vector<Func> funcs;

    public Program() {
        funcs = new Vector<Func>();
    }

    public void SetMain(MainFunc main) {
        mainFunc = main;
    }

    public void AddFunc(Func func) {
        funcs.addElement(func);
    }
    public void MergeFuncList(FuncsList funcsList) {
        for (int i = 0, sz = funcsList.funcs.size(); i < sz; i++) {
            funcs.addElement(funcsList.funcs.get(i));
        }
    }

    @Override
    public void print(PrintStream out, int depth) {
        mainFunc.print(out, depth);
        out.println();
        for (int i = 0, sz = funcs.size(); i < sz; i++) {
            funcs.get(i).print(out, depth);
            out.println();
        }
    }
}

package minijavac.piglet.exp;

public abstract class Stmt extends Exp {
    private String label;
    public Stmt() {
        label = null;
    }

    public void SetLabel(String _label) {
        label = _label;
    }
    public String GetLabel() {
        return label;
    }
}

package minijavac.piglet.exp;

import minijavac.ErrorHandler;

import java.io.PrintStream;
import java.util.Vector;

public class StmtList extends Exp {
    public Vector<Stmt> stmts;

    public StmtList(Exp... _stmts) {
        stmts = new Vector<>();
        for (int i = 0; i < _stmts.length; i++) {
            if (_stmts[i] instanceof StmtList) {
                StmtList list = (StmtList)_stmts[i];
                for (int j = 0, sz = list.stmts.size(); j < sz; j++) {
                    addStmt(list.stmts.get(j));
                }
            } else if (_stmts[i] instanceof Stmt){
                addStmt((Stmt)_stmts[i]);
            } else {
                ErrorHandler.PigletError("try to add a exp(not stmt) to StmtList");
            }
        }
    }

    public void addStmt(Stmt stmt) {
        stmts.addElement(stmt);
    }
    public Stmt[] toStmtArray() {
        Stmt[] stmtArray = new Stmt[stmts.size()];
        for (int i = 0; i < stmtArray.length; i++)
            stmtArray[i] = stmts.get(i);
        return stmtArray;
    }
    public void addStmtList(StmtList stmtList) {
        for (int i = 0, sz = stmtList.stmts.size(); i < sz; i++) {
            addStmt(stmtList.stmts.get(i));
        }
    }

    @Override
    public void print(PrintStream out, int depth) {
        ErrorHandler.PigletError("should never call StmtList.print()");
    }
    public void printDebug(PrintStream out, int depth) {
        for (int i = 0, sz = stmts.size(); i < sz; i++) {
            stmts.get(i).print(out, depth);
            out.println();
        }
    }
}

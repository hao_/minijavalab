package minijavac.piglet.exp;

import java.io.PrintStream;

public class Symbol extends Stmt {
    String symbol;

    public Symbol(String _symbol) {
        symbol = _symbol;
    }

    @Override
    public void print(PrintStream out, int depth) {
        out.printf("%s", symbol);
    }
}

package minijavac.piglet.exp;

import java.io.PrintStream;

public class Temp extends Stmt {
    public int tempUnit;

    public Temp(int _tempUnit) {
        tempUnit = _tempUnit;
    }

    @Override
    public void print(PrintStream out, int depth) {
        out.printf("TEMP %d", tempUnit);
    }
}

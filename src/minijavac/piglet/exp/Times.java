package minijavac.piglet.exp;

import java.io.PrintStream;

public class Times extends Stmt {
    public Exp exp1, exp2;

    public Times(Exp _exp1, Exp _exp2) {
        exp1 = _exp1;
        exp2 = _exp2;
    }

    @Override
    public void print(PrintStream out, int depth) {
        out.print("TIMES ");
        exp1.print(out, depth);
        out.print(" ");
        exp2.print(out, depth);
    }
}

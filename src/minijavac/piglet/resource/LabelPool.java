package minijavac.piglet.resource;

public class LabelPool {
    private int currentLabelNum;

    public LabelPool() {
        currentLabelNum = 0;
    }

    public String New() {
        return String.format("L%d", currentLabelNum++);
    }
}

package minijavac.piglet.resource;

import minijavac.Specification;

import java.util.TreeSet;

public class TempUnitPool {
    private int currentTemp;
    private TreeSet<Integer> recyclePool;

    private void recycle(int t) {
        recyclePool.add(t);
    }

    public TempUnitPool() {
        currentTemp = Specification.PigletFirstTempUnit;
        recyclePool = new TreeSet<>();
    }

    public TempUnitPool(int minTemp) {
        currentTemp = Specification.PigletFirstTempUnit;
        if (minTemp > currentTemp) {
            currentTemp = minTemp;
        }
        recyclePool = new TreeSet<>();
    }

    public int New() {
        if (recyclePool != null && !recyclePool.isEmpty()) {
            Integer u = recyclePool.first();
            recyclePool.remove(u);
            return u;
        }
        return currentTemp++;
    }
    public void Recycle(int... units) {
        for (int u : units) {
            recycle(u);
        }
    }
}

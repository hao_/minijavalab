package minijavac.piglet.visitors;

import minijavac.piglet.JTBvisitor.DepthFirstVisitor;
import minijavac.piglet.parser.Node;
import minijavac.piglet.parser.Temp;

public class GetRegisterInfoVisitor extends DepthFirstVisitor {
    private int maxTemp;

    private GetRegisterInfoVisitor() {
        maxTemp = 0;
    }

    public int getMaxTemp() {
        return maxTemp;
    }

    public static int VisitorEntry(Node goal) {
        GetRegisterInfoVisitor visitor = new GetRegisterInfoVisitor();
        goal.accept(visitor);
        return visitor.getMaxTemp();
    }

    /**
     * f0 -> "TEMP"
     * f1 -> IntegerLiteral()
     */
    public void visit(Temp n) {
        Integer temp = Integer.valueOf(n.f1.f0.toString());
        if (temp > maxTemp) {
            maxTemp = temp;
        }
    }
}

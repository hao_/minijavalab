package minijavac.piglet.visitors;

import minijavac.ErrorHandler;
import minijavac.Specification;
import minijavac.piglet.JTBvisitor.GJDepthFirst;
import minijavac.piglet.parser.*;
import minijavac.spiglet.SpigletProgram;

import java.util.Enumeration;

interface VisitorRet{}
abstract class VisitorArgu{}

class ArguNeedTemp extends VisitorArgu implements VisitorRet {
    private int temp;
    ArguNeedTemp() {
        temp = -1;
    }
    void SetTemp(int t) {
        temp = t;
    }
    int GetTemp() {
        return temp;
    }
}

class ArguExp  extends VisitorArgu implements VisitorRet {
    private String exp;
    ArguExp() {
        exp = null;
    }
    void SetExp(String e) {
        exp = e;
    }
    String GetExp() {
        return exp;
    }
}

class ArguSimExp extends VisitorArgu implements VisitorRet {
    private String exp;
    ArguSimExp() {
        exp = null;
    }
    void SetSimExp(String e) {
        exp = e;
    }
    String GetSimExp() {
        return exp;
    }
}

class ArguProcBody extends VisitorArgu implements VisitorRet{
}

class SpigletContext {
    private SpigletProgram program;
    private String label;
    void setLabel(String _label) {
        label = _label;
    }
    SpigletContext(SpigletProgram _program) {
        program = _program;
        label = null;
    }
    void addLine(String line) {
        program.AddProgramLine(line);
    }
    void addStmtLine(String line) {
        if (label == null) {
            addLine(String.format("%s%s", Specification.PigletIndent, line));
        } else {
            addLine(String.format("%s%s", Specification.PigletLabeledIndent(label), line));
            label = null;
        }
    }
    int newTemp() {
        return program.tempUnitPool.New();
    }
    void recycleTemp(int t) {
        /*
        if (t >= program.minTemp)
            program.tempUnitPool.Recycle(t);*/
    }
}

public class ToSpigletVisitor extends GJDepthFirst<VisitorRet, VisitorArgu> {
    private SpigletContext context;

    private ToSpigletVisitor(SpigletProgram program) {
        context = new SpigletContext(program);
    }

    public static void VisitorEntry(Node goal, SpigletProgram program) {
        goal.accept(new ToSpigletVisitor(program), null);
    }

    /**
     * f0 -> "MAIN"
     * f1 -> StmtList()
     * f2 -> "END"
     * f3 -> ( SProcedure() )*
     * f4 -> <EOF>
     */
    public VisitorRet visit(Goal n, VisitorArgu argu) {
        context.addLine("MAIN");
        n.f1.accept(this, null);
        context.addLine("END");
        context.addLine("");
        n.f3.accept(this, null);
        return null;
    }



    /**
     * f0 -> ( ( Label() )? Stmt() )*
     */
    public VisitorRet visit(StmtList n, VisitorArgu argu) {
        if (n.f0.present()) {
            for ( Enumeration<Node> e = n.f0.elements(); e.hasMoreElements(); ) {
                NodeSequence n1 = (NodeSequence)e.nextElement();
                NodeOptional labeln = (NodeOptional)n1.elementAt(0);
                Node stmtn = n1.elementAt(1);

                if (labeln.present()) {
                    String label = ((Label) labeln.node).f0.toString();
                    context.setLabel(label);
                }
                stmtn.accept(this, argu);
            }
        }
        return null;
    }


    /**
     * f0 -> Label()
     * f1 -> "["
     * f2 -> IntegerLiteral()
     * f3 -> "]"
     * f4 -> StmtExp()
     */
    public VisitorRet visit(Procedure n, VisitorArgu argu) {
        context.addLine(String.format("%s [%d]",
                n.f0.f0.toString(),
                Integer.valueOf(n.f2.f0.toString())
        ));
        context.addLine("BEGIN");
        n.f4.accept(this, new ArguProcBody());
        context.addLine("END");
        return null;
    }

    /**
     * f0 -> NoOpStmt()
     *       | ErrorStmt()
     *       | CJumpStmt()
     *       | JumpStmt()
     *       | HStoreStmt()
     *       | HLoadStmt()
     *       | MoveStmt()
     *       | PrintStmt()
     */
    public VisitorRet visit(Stmt n, VisitorArgu argu) {
        n.f0.accept(this, null);
        return null;
    }

    /**
     * f0 -> "NOOP"
     */
    public VisitorRet visit(NoOpStmt n, VisitorArgu argu) {
        context.addStmtLine("NOOP");
        return null;
    }

    /**
     * f0 -> "ERROR"
     */
    public VisitorRet visit(ErrorStmt n, VisitorArgu argu) {
        context.addStmtLine("ERROR");
        return null;
    }

    /**
     * f0 -> "CJUMP"
     * f1 -> Exp()
     * f2 -> Label()
     */
    public VisitorRet visit(CJumpStmt n, VisitorArgu argu) {
        ArguNeedTemp exp = (ArguNeedTemp)n.f1.accept(this, new ArguNeedTemp());
        context.addStmtLine(String.format(
                "CJUMP TEMP %d %s",
                exp.GetTemp(),
                n.f2.f0.toString()
        ));
        context.recycleTemp(exp.GetTemp());
        return null;
    }

    /**
     * f0 -> "JUMP"
     * f1 -> Label()
     */
    public VisitorRet visit(JumpStmt n, VisitorArgu argu) {
        context.addStmtLine(String.format("JUMP %s", n.f1.f0.toString()));
        return null;
    }

    /**
     * f0 -> "HSTORE"
     * f1 -> Exp()
     * f2 -> IntegerLiteral()
     * f3 -> Exp()
     */
    public VisitorRet visit(HStoreStmt n, VisitorArgu argu) {
        ArguNeedTemp exp1 = (ArguNeedTemp)n.f1.accept(this, new ArguNeedTemp());
        ArguNeedTemp exp2 = (ArguNeedTemp)n.f3.accept(this, new ArguNeedTemp());
        context.addStmtLine(String.format(
                "HSTORE TEMP %d %d TEMP %d",
                exp1.GetTemp(),
                Integer.valueOf(n.f2.f0.toString()),
                exp2.GetTemp()
        ));
        context.recycleTemp(exp1.GetTemp());
        context.recycleTemp(exp2.GetTemp());
        return null;
    }

    /**
     * f0 -> "HLOAD"
     * f1 -> Temp()
     * f2 -> Exp()
     * f3 -> IntegerLiteral()
     */
    public VisitorRet visit(HLoadStmt n, VisitorArgu argu) {
        ArguNeedTemp exp1 = (ArguNeedTemp)n.f1.accept(this, new ArguNeedTemp());
        ArguNeedTemp exp2 = (ArguNeedTemp)n.f2.accept(this, new ArguNeedTemp());
        context.addStmtLine(String.format(
                "HLOAD TEMP %d TEMP %d %d",
                exp1.GetTemp(),
                exp2.GetTemp(),
                Integer.valueOf(n.f3.f0.toString())
        ));
        context.recycleTemp(exp1.GetTemp());
        context.recycleTemp(exp2.GetTemp());
        return null;
    }

    /**
     * f0 -> "MOVE"
     * f1 -> Temp()
     * f2 -> Exp()
     */
    public VisitorRet visit(MoveStmt n, VisitorArgu argu) {
        ArguNeedTemp temp = (ArguNeedTemp)n.f1.accept(this, new ArguNeedTemp());
        ArguExp exp = (ArguExp) n.f2.accept(this, new ArguExp());
        context.addStmtLine(String.format(
                "MOVE TEMP %d %s",
                temp.GetTemp(),
                exp.GetExp()
        ));
        context.recycleTemp(temp.GetTemp());
        return null;
    }

    /**
     * f0 -> "PRINT"
     * f1 -> Exp()
     */
    public VisitorRet visit(PrintStmt n, VisitorArgu argu) {
        ArguSimExp simExp = (ArguSimExp)n.f1.accept(this, new ArguSimExp());
        context.addStmtLine(String.format(
                "PRINT %s",
                simExp.GetSimExp()
        ));
        return null;
    }

    /**
     * f0 -> StmtExp()
     *       | Call()
     *       | HAllocate()
     *       | BinOp()
     *       | Temp()
     *       | IntegerLiteral()
     *       | Label()
     */
    public VisitorRet visit(Exp n, VisitorArgu argu) {
        return n.f0.accept(this, argu);
    }

    /**
     * f0 -> "BEGIN"
     * f1 -> StmtList()
     * f2 -> "RETURN"
     * f3 -> Exp()
     * f4 -> "END"
     */
    public VisitorRet visit(StmtExp n, VisitorArgu argu) {
        n.f1.accept(this, null);
        if (argu instanceof ArguExp || argu instanceof ArguSimExp || argu instanceof ArguNeedTemp) {
            return n.f3.accept(this, argu);
        } else if (argu instanceof ArguProcBody) {
            ArguSimExp simExp = (ArguSimExp)n.f3.accept(this, new ArguSimExp());
            context.addStmtLine(String.format(
                    "RETURN %s",
                    simExp.GetSimExp()
            ));
            return null;
        } else {
            ErrorHandler.SpigletError("Unknown StmtExp Node");
            return null;
        }
    }

    /**
     * f0 -> "CALL"
     * f1 -> Exp()
     * f2 -> "("
     * f3 -> ( Exp() )*
     * f4 -> ")"
     */
    public VisitorRet visit(Call n, VisitorArgu argu) {
        ArguSimExp exp1 = (ArguSimExp)n.f1.accept(this, new ArguSimExp());
        String paraList = "";
        if (n.f3.present()) {
            for (Enumeration<Node> e = n.f3.elements(); e.hasMoreElements(); ) {
                Node ne = e.nextElement();
                ArguExp exp = (ArguExp)ne.accept(this, new ArguExp());
                int temp = context.newTemp();
                paraList = String.format("%s TEMP %d", paraList, temp);
                context.addStmtLine(String.format(
                        "MOVE TEMP %d %s",
                        temp,
                        exp.GetExp()
                ));
            }
        }

        if (argu instanceof ArguSimExp || argu instanceof ArguNeedTemp) {
            int temp = context.newTemp();
            context.addStmtLine(String.format(
                    "MOVE TEMP %d CALL %s (%s)",
                    temp,
                    exp1.GetSimExp(),
                    paraList
            ));
            if (argu instanceof ArguSimExp) {
                ((ArguSimExp) argu).SetSimExp(String.format("TEMP %d", temp));
            } else {
                ((ArguNeedTemp) argu).SetTemp(temp);
            }
        } else if (argu instanceof ArguExp) {
            ((ArguExp) argu).SetExp(String.format(
                    "CALL %s (%s)",
                    exp1.GetSimExp(),
                    paraList
            ));
        } else {
            ErrorHandler.SpigletError("Unknown call node");
            return null;
        }

        return (VisitorRet)argu;
    }

    /**
     * f0 -> "HALLOCATE"
     * f1 -> Exp()
     */
    public VisitorRet visit(HAllocate n, VisitorArgu argu) {
        ArguSimExp exp = (ArguSimExp)n.f1.accept(this, new ArguSimExp());

        if (argu instanceof ArguSimExp || argu instanceof ArguNeedTemp) {
            int temp = context.newTemp();
            context.addStmtLine(String.format(
                    "MOVE TEMP %d HALLOCATE %s",
                    temp,
                    exp.GetSimExp()
            ));

            if (argu instanceof ArguSimExp) {
                ((ArguSimExp) argu).SetSimExp(String.format("TEMP %d", temp));
            } else {
                ((ArguNeedTemp) argu).SetTemp(temp);
            }
        } else if (argu instanceof ArguExp) {
            ((ArguExp) argu).SetExp(String.format(
                    "HALLOCATE %s",
                    exp.GetSimExp()
            ));
        } else {
            ErrorHandler.SpigletError("Unknown Hallocate node");
            return null;
        }
        return (VisitorRet)argu;
    }

    /**
     * f0 -> Operator()
     * f1 -> Exp()
     * f2 -> Exp()
     */
    public VisitorRet visit(BinOp n, VisitorArgu argu) {
        ArguNeedTemp exp1 = (ArguNeedTemp)n.f1.accept(this, new ArguNeedTemp());
        ArguSimExp exp2 = (ArguSimExp)n.f2.accept(this, new ArguSimExp());
        String opr = n.f0.f0.choice.toString();

        if (argu instanceof ArguSimExp || argu instanceof ArguNeedTemp) {
            int temp = context.newTemp();
            context.addStmtLine(String.format(
                    "MOVE TEMP %d %s TEMP %d %s",
                    temp,
                    opr,
                    exp1.GetTemp(),
                    exp2.GetSimExp()
            ));

            if (argu instanceof ArguSimExp) {
                ((ArguSimExp) argu).SetSimExp(String.format("TEMP %d", temp));
            } else {
                ((ArguNeedTemp) argu).SetTemp(temp);
            }
        } else if (argu instanceof ArguExp) {
            ((ArguExp) argu).SetExp(String.format(
                    "%s TEMP %d %s",
                    opr,
                    exp1.GetTemp(),
                    exp2.GetSimExp()
            ));
        } else {
            ErrorHandler.SpigletError("Unknown binop node");
            return null;
        }

        context.recycleTemp(exp1.GetTemp());
        return (VisitorRet)argu;
    }

    /**
     * f0 -> "TEMP"
     * f1 -> IntegerLiteral()
     */
    public VisitorRet visit(Temp n, VisitorArgu argu) {
        int temp = Integer.valueOf(n.f1.f0.toString());
        String ret = String.format("TEMP %s", n.f1.f0.toString());

        if (argu instanceof ArguSimExp) {
            ((ArguSimExp) argu).SetSimExp(ret);
        } else if (argu instanceof ArguExp) {
            ((ArguExp) argu).SetExp(ret);
        } else if (argu instanceof ArguNeedTemp) {
            ((ArguNeedTemp) argu).SetTemp(temp);
        } else {
            ErrorHandler.SpigletError("Unknown temp node");
            return null;
        }
        return (VisitorRet) argu;
    }

    /**
     * f0 -> <INTEGER_LITERAL>
     */
    public VisitorRet visit(IntegerLiteral n, VisitorArgu argu) {
        String ret = n.f0.toString();

        if (argu instanceof ArguSimExp) {
            ((ArguSimExp) argu).SetSimExp(ret);
        } else if (argu instanceof ArguExp) {
            ((ArguExp) argu).SetExp(ret);
        } else if (argu instanceof ArguNeedTemp) {
            int temp = context.newTemp();
            context.addStmtLine(String.format("MOVE TEMP %d %s", temp, ret));
            ((ArguNeedTemp) argu).SetTemp(temp);
        } else {
            ErrorHandler.SpigletError("Unknown integerLiteral node");
            return null;
        }
        return (VisitorRet) argu;
    }

    /**
     * f0 -> <IDENTIFIER>
     */
    public VisitorRet visit(Label n, VisitorArgu argu) {
        String ret = n.f0.toString();

        if (argu instanceof ArguSimExp) {
            ((ArguSimExp) argu).SetSimExp(ret);
        } else if (argu instanceof ArguExp) {
            ((ArguExp) argu).SetExp(ret);
        } else if (argu instanceof ArguNeedTemp) {
            int temp = context.newTemp();
            context.addStmtLine(String.format("MOVE TEMP %d %s", temp, ret));
            ((ArguNeedTemp) argu).SetTemp(temp);
        } else {
            ErrorHandler.SpigletError("Unknown label node");
            return null;
        }
        return (VisitorRet) argu;
    }
}
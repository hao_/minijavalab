package minijavac.spiglet;

import minijavac.piglet.parser.Node;
import minijavac.piglet.parser.ParseException;
import minijavac.piglet.parser.PigletParser;
import minijavac.piglet.resource.LabelPool;
import minijavac.piglet.resource.TempUnitPool;
import minijavac.piglet.visitors.GetRegisterInfoVisitor;
import minijavac.piglet.visitors.ToSpigletVisitor;

import java.io.PrintStream;
import java.util.Vector;

public class SpigletProgram {
    public LabelPool labelPool;
    public TempUnitPool tempUnitPool;
    public Vector<String> program;
    public int minTemp;

    public SpigletProgram() {
        labelPool = new LabelPool();
        tempUnitPool = new TempUnitPool();
        program = new Vector<>();
    }

    public void FromPiglet(PigletParser parser) {
        try {
            Node goal = parser.Goal();
            minTemp = GetRegisterInfoVisitor.VisitorEntry(goal) + 1;
            tempUnitPool = new TempUnitPool(minTemp);
            ToSpigletVisitor.VisitorEntry(goal, this);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void Print(PrintStream out) {
        for (String line : program) {
            out.println(line);
        }
    }

    public void AddProgramLine(String line) {
        program.addElement(line);
    }
}
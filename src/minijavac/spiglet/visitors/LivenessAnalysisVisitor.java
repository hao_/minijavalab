package minijavac.spiglet.visitors;

import minijavac.ErrorHandler;
import minijavac.spiglet.JTBvisitor.GJNoArguDepthFirst;
import minijavac.spiglet.parser.*;
import minijavac.util.StmtConstraint;
import minijavac.util.StmtConstraintList;

import java.util.*;



public class LivenessAnalysisVisitor extends GJNoArguDepthFirst<StmtConstraint> {
    private StmtConstraintList cons;

    private LivenessAnalysisVisitor(StmtConstraintList _cons) {
        cons = _cons;
    }

    public static StmtConstraintList VisitorEntry(Node node) {
        if (!(node instanceof StmtList) && !(node instanceof StmtExp)) {
            ErrorHandler.KangaError("Only StmtList or StmtExp can accept LivenessAnalysisVisitor entry");
        }

        StmtConstraintList cons = new StmtConstraintList();

        node.accept(new LivenessAnalysisVisitor(cons));
        StmtConstraint exitCon = new StmtConstraint();
        exitCon.setNoThrough();
        cons.addCon(exitCon);

        cons.livenessAnalyse();

        return cons;
    }

    private int getTempNum(Temp tmp) {
        return Integer.valueOf(tmp.f1.f0.toString());
    }

    private String getLabel(Label label) {
        return label.f0.toString();
    }

    /**
     * f0 -> ( ( Label() )? Stmt() )*
     */
    public StmtConstraint visit(StmtList n) {
        for ( Enumeration<Node> e = n.f0.elements(); e.hasMoreElements(); ) {
            NodeSequence n1 = (NodeSequence)e.nextElement();
            NodeOptional labeln = (NodeOptional)n1.elementAt(0);
            Node stmtn = n1.elementAt(1);
            StmtConstraint con = stmtn.accept(this);

            if (labeln.present()) {
                String label = ((Label) labeln.node).f0.toString();
                cons.addConWithLabel(con, label);
            } else {
                cons.addCon(con);
            }
        }
        return null;
    }

    /**
     * f0 -> "BEGIN"
     * f1 -> StmtList()
     * f2 -> "RETURN"
     * f3 -> SimpleExp()
     * f4 -> "END"
     */
    public StmtConstraint visit(StmtExp n) {
        n.f1.accept(this);
        cons.addCon(n.f3.accept(this));
        return null;
    }

    /**
     * f0 -> NoOpStmt()
     *       | ErrorStmt()
     *       | CJumpStmt()
     *       | JumpStmt()
     *       | HStoreStmt()
     *       | HLoadStmt()
     *       | MoveStmt()
     *       | PrintStmt()
     */
    public StmtConstraint visit(Stmt n) {
        return n.f0.accept(this);
    }

    /**
     * f0 -> "NOOP"
     */
    public StmtConstraint visit(NoOpStmt n) {
        return new StmtConstraint();
    }

    /**
     * f0 -> "ERROR"
     */
    public StmtConstraint visit(ErrorStmt n) {
        StmtConstraint c = new StmtConstraint();
        c.setNoThrough();
        return c;
    }

    /**
     * f0 -> "CJUMP"
     * f1 -> Temp()
     * f2 -> Label()
     */
    public StmtConstraint visit(CJumpStmt n) {
        StmtConstraint c = new StmtConstraint();
        c.addUse(getTempNum(n.f1));
        c.addNext(getLabel(n.f2));
        return c;
    }

    /**
     * f0 -> "JUMP"
     * f1 -> Label()
     */
    public StmtConstraint visit(JumpStmt n) {
        StmtConstraint c = new StmtConstraint();
        c.addNext(getLabel(n.f1));
        c.setNoThrough();
        return c;
    }

    /**
     * f0 -> "HSTORE"
     * f1 -> Temp()
     * f2 -> IntegerLiteral()
     * f3 -> Temp()
     */
    public StmtConstraint visit(HStoreStmt n) {
        StmtConstraint c = new StmtConstraint();
        c.addUse(getTempNum(n.f1));
        c.addUse(getTempNum(n.f3));
        return c;
    }

    /**
     * f0 -> "HLOAD"
     * f1 -> Temp()
     * f2 -> Temp()
     * f3 -> IntegerLiteral()
     */
    public StmtConstraint visit(HLoadStmt n) {
        StmtConstraint c = new StmtConstraint();
        c.addKill(getTempNum(n.f1));
        c.addUse(getTempNum(n.f2));
        return c;
    }

    /**
     * f0 -> "MOVE"
     * f1 -> Temp()
     * f2 -> Exp()
     */
    public StmtConstraint visit(MoveStmt n) {
        StmtConstraint c = new StmtConstraint();
        c.addKill(getTempNum(n.f1));
        c.merge(n.f2.accept(this));
        return c;
    }

    /**
     * f0 -> "PRINT"
     * f1 -> SimpleExp()
     */
    public StmtConstraint visit(PrintStmt n) {
        return n.f1.accept(this);
    }

    /**
     * f0 -> Call()
     *       | HAllocate()
     *       | BinOp()
     *       | SimpleExp()
     */
    public StmtConstraint visit(Exp n) {
        return n.f0.accept(this);
    }

    /**
     * f0 -> "CALL"
     * f1 -> SimpleExp()
     * f2 -> "("
     * f3 -> ( Temp() )*
     * f4 -> ")"
     */
    public StmtConstraint visit(Call n) {
        StmtConstraint c = new StmtConstraint();
        c.merge(n.f1.accept(this));
        if (n.f3.present()) {
            for ( Enumeration<Node> e = n.f3.elements(); e.hasMoreElements(); ) {
                c.addUse(getTempNum( ((Temp) e.nextElement()) ));
            }
        }
        return c;
    }

    /**
     * f0 -> "HALLOCATE"
     * f1 -> SimpleExp()
     */
    public StmtConstraint visit(HAllocate n) {
        return n.f1.accept(this);
    }

    /**
     * f0 -> Operator()
     * f1 -> Temp()
     * f2 -> SimpleExp()
     */
    public StmtConstraint visit(BinOp n) {
        StmtConstraint c = new StmtConstraint();
        c.addUse(getTempNum(n.f1));
        c.merge(n.f2.accept(this));
        return c;
    }

    /**
     * f0 -> Temp()
     *       | IntegerLiteral()
     *       | Label()
     */
    public StmtConstraint visit(SimpleExp n) {
        StmtConstraint c = new StmtConstraint();
        if (n.f0.which == 0) {
            c.addUse(getTempNum((Temp)n.f0.choice));
        }
        return c;
    }

    /**
     * f0 -> "TEMP"
     * f1 -> IntegerLiteral()
     */
    public StmtConstraint visit(Temp n) {
        StmtConstraint c = new StmtConstraint();
        c.addUse(getTempNum(n));
        return c;
    }
}

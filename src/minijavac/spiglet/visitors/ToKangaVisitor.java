package minijavac.spiglet.visitors;

import minijavac.ErrorHandler;
import minijavac.kanga.KangaProgram;
import minijavac.kanga.KangaSpecification;
import minijavac.kanga.KgProcedure;
import minijavac.spiglet.JTBvisitor.GJNoArguDepthFirst;
import minijavac.spiglet.parser.*;
import minijavac.util.StmtConstraintList;

import java.util.*;

class ProcContext {
    Map<Integer, Integer> tempMap;
    int tempN, arguNum, arguNumStack, nowStmt;
    StmtConstraintList cons;

    ProcContext(Map<Integer, Integer> _tempMap, int _arguNum, StmtConstraintList _cons) {
        tempMap = _tempMap;
        tempN = -1;
        for (Map.Entry<Integer, Integer> i: tempMap.entrySet()) {
            int k = i.getValue();
            if (k > tempN) {
                tempN = k;
            }
        }
        tempN++;
        arguNum = _arguNum;
        arguNumStack = arguNum - KangaSpecification.arguReg.length;
        if (arguNumStack < 0) {
            arguNumStack = 0;
        }
        nowStmt = 0;
        cons = _cons;
    }

    int getSpillNum(boolean isMain) {
        if (isMain) {
            if (tempN > KangaSpecification.calleeSaver.length) {
                return tempN - KangaSpecification.calleeSaver.length;
            } else {
                return 0;
            }
        }
        return tempN + arguNumStack;
    }

    void storeCalleeSave(KgProcedure proc) {
        int s = tempN;
        if (s > KangaSpecification.calleeSaver.length) {
            s = KangaSpecification.calleeSaver.length;
        }
        for (int i = 0; i < s; i++) {
            proc.addInitStmt(String.format("ASTORE SPILLEDARG %d %s",
                    i + arguNumStack, KangaSpecification.calleeSaver[i]));
            proc.addStmt(String.format("ALOAD %s SPILLEDARG %d",
                    KangaSpecification.calleeSaver[i], i + arguNumStack));
        }
    }

    String getTemp(int temp, KgProcedure proc, int tmpReg) {
        int n = tempMap.get(temp);
        if (n < KangaSpecification.calleeSaver.length) {
            return KangaSpecification.calleeSaver[n];
        }
        n -= KangaSpecification.calleeSaver.length;
        if (n < KangaSpecification.callerSaver.length) {
            return KangaSpecification.callerSaver[n];
        }
        n += KangaSpecification.calleeSaver.length;

        String reg;
        switch (tmpReg) {
            case 0:
                reg = KangaSpecification.tmpReg;
                break;
            case 1:
                reg = KangaSpecification.returnReg;
                break;
            default:
                ErrorHandler.KangaError("getTemp: wrong tmpReg");
                return "";
        }
        proc.addStmt(String.format("ALOAD %s SPILLEDARG %d", reg, n + arguNumStack));
        return reg;
    }

    void saveTemp(KgProcedure proc, int temp, String command, String exp, boolean canCut) {
        if (!tempMap.containsKey(temp)) {
            // No need to save to useless temp
            proc.addStmt("NOOP");
            return;
        }
        boolean cut = false;
        if (canCut && nowStmt + 1 < cons.consList.size() && !cons.consList.get(nowStmt + 1).live.contains(temp)
                && !cons.consList.get(nowStmt + 1).use.contains(temp)) {
            proc.addStmt("NOOP");
            //cut = true;
            return;
        }
        int n = tempMap.get(temp);
        if (n < KangaSpecification.calleeSaver.length) {
            proc.addStmt(String.format("%s %s %s", command, KangaSpecification.calleeSaver[n], exp));
            //if (cut) proc.addStmt("Cut finish");
            return;
        }
        n -= KangaSpecification.calleeSaver.length;
        if (n < KangaSpecification.callerSaver.length) {
            proc.addStmt(String.format("%s %s %s", command, KangaSpecification.callerSaver[n], exp));
            //if (cut) proc.addStmt("Cut finish");
            return;
        }
        n += KangaSpecification.calleeSaver.length;

        proc.addStmt(String.format("%s %s %s", command, KangaSpecification.tmpResReg, exp));
        proc.addStmt(String.format("ASTORE SPILLEDARG %d %s", n + arguNumStack, KangaSpecification.tmpResReg));
        //if (cut) proc.addStmt("Cut finish");
    }

    private String getArgu(KgProcedure proc, int k) {
        if (k < KangaSpecification.arguReg.length) {
            return KangaSpecification.arguReg[k];
        }
        proc.addStmt(String.format("ALOAD %s SPILLEDARG %d",
                KangaSpecification.tmpReg, k - KangaSpecification.arguReg.length));
        return KangaSpecification.tmpReg;
    }

    void getArgus(KgProcedure proc) {
        for (int i = 0; i < arguNum; i++) {
            String reg = getArgu(proc, i);
            saveTemp(proc, i, "MOVE", reg, false);
        }
    }

    void callFunc(KgProcedure proc) {
        int lim = tempN - KangaSpecification.calleeSaver.length;
        for (int i = 0; i < lim && i < KangaSpecification.callerSaver.length; i++) {
            proc.addStmt(String.format("ASTORE SPILLEDARG %d %s",
                    i + KangaSpecification.calleeSaver.length + arguNumStack,
                    KangaSpecification.callerSaver[i]
            ));
        }
    }
    void callFuncFin(KgProcedure proc) {
        int lim = tempN - KangaSpecification.calleeSaver.length;
        for (int i = 0; i < lim && i < KangaSpecification.callerSaver.length; i++) {
            proc.addStmt(String.format("ALOAD %s SPILLEDARG %d",
                    KangaSpecification.callerSaver[i],
                    i + KangaSpecification.calleeSaver.length + arguNumStack
            ));
        }
    }
}

public class ToKangaVisitor extends GJNoArguDepthFirst<String> {
    private KangaProgram kangaProgram;
    private StmtConstraintList cons;
    private KgProcedure proc;
    private ProcContext context;
    private int _globalnum;
    private int maxcall;

    private ToKangaVisitor(KangaProgram _kangaProgram) {
        kangaProgram = _kangaProgram;
    }

    public static void VisitorEntry(Node goal, KangaProgram kangaProgram) {
        goal.accept(new ToKangaVisitor(kangaProgram));
    }

    private int getTemp(Temp n) {
        return Integer.valueOf(n.f1.f0.toString());
    }

    /**
     * f0 -> "MAIN"
     * f1 -> StmtList()
     * f2 -> "END"
     * f3 -> ( Procedure() )*
     * f4 -> <EOF>
     */
    public String visit(Goal n) {
        proc = new KgProcedure(n.f0.toString());
        if (!kangaProgram.addProcedure(proc)) {
            ErrorHandler.KangaError(String.format("Failed to add procedure %s", n.f0.toString()));
        }
        cons = LivenessAnalysisVisitor.VisitorEntry(n.f1);

        // Translate main procedure
        context = new ProcContext(cons.colorTemp(), 0, cons);
        maxcall = 0;

        context.getArgus(proc);
        n.f1.accept(this);
        proc.argnum = 0;
        proc.spillnum = context.getSpillNum(true);
        proc.maxcallarg = maxcall;

        // Translate other procedure
        n.f3.accept(this);

        return "";
    }

    /**
     * f0 -> ( ( Label() )? Stmt() )*
     */
    public String visit(StmtList n) {
        for (Enumeration<Node> e = n.f0.elements(); e.hasMoreElements(); ) {
            NodeSequence n1 = (NodeSequence)e.nextElement();
            NodeOptional labeln = (NodeOptional)n1.elementAt(0);
            Node stmtn = n1.elementAt(1);

            if (labeln.present()) {
                String label = ((Label) labeln.node).f0.toString();
                proc.setLabel(label);
            }
            String stmt = stmtn.accept(this);
            if (stmt != "") {
                proc.addStmt(stmt);
            }
        }
        return "";
    }

    /**
     * f0 -> Label()
     * f1 -> "["
     * f2 -> IntegerLiteral()
     * f3 -> "]"
     * f4 -> StmtExp()
     */
    public String visit(Procedure n) {
        proc = new KgProcedure(n.f0.f0.toString());
        if (!kangaProgram.addProcedure(proc)) {
            ErrorHandler.KangaError(String.format("Failed to add procedure %s", n.f0.toString()));
        }

        cons = LivenessAnalysisVisitor.VisitorEntry(n.f4);

        proc.argnum = Integer.valueOf(n.f2.accept(this));
        context = new ProcContext(cons.colorTemp(), proc.argnum, cons);
        maxcall = 0;

        //System.out.printf("%s %s\n", n.f0.f0.toString(), context.tempMap);
        //cons.printLive(System.out);

        context.getArgus(proc);
        n.f4.accept(this);

        // Store and restore callee-save
        context.storeCalleeSave(proc);

        proc.spillnum = context.getSpillNum(false);
        proc.maxcallarg = maxcall;
        return "";
    }

    /**
     * f0 -> "BEGIN"
     * f1 -> StmtList()
     * f2 -> "RETURN"
     * f3 -> SimpleExp()
     * f4 -> "END"
     */
    public String visit(StmtExp n) {
        n.f1.accept(this);
        proc.addStmt(String.format("MOVE %s %s", KangaSpecification.returnReg, n.f3.accept(this)));
        return "";
    }

    /**
     * f0 -> NoOpStmt()
     *       | ErrorStmt()
     *       | CJumpStmt()
     *       | JumpStmt()
     *       | HStoreStmt()
     *       | HLoadStmt()
     *       | MoveStmt()
     *       | PrintStmt()
     */
    public String visit(Stmt n) {
        String res = n.f0.accept(this);
        context.nowStmt++;
        return res;
    }

    /**
     * f0 -> "NOOP"
     */
    public String visit(NoOpStmt n) {
        proc.addStmt(n.f0.toString());
        return "";
    }

    /**
     * f0 -> "ERROR"
     */
    public String visit(ErrorStmt n) {
        proc.addStmt(n.f0.toString());
        return "";
    }

    /**
     * f0 -> "CJUMP"
     * f1 -> Temp()
     * f2 -> Label()
     */
    public String visit(CJumpStmt n) {
        int temp = getTemp(n.f1);
        return String.format("%s %s %s",
                n.f0.toString(),
                context.getTemp(temp, proc, 0),
                n.f2.accept(this)
        );
    }

    /**
     * f0 -> "JUMP"
     * f1 -> Label()
     */
    public String visit(JumpStmt n) {
        return String.format("%s %s", n.f0.toString(), n.f1.accept(this));
    }

    /**
     * f0 -> "HSTORE"
     * f1 -> Temp()
     * f2 -> IntegerLiteral()
     * f3 -> Temp()
     */
    public String visit(HStoreStmt n) {
        int temp = getTemp(n.f1);
        int tempexp = getTemp(n.f3);

        proc.addStmt(String.format("%s %s %s %s",
                n.f0.toString(),
                context.getTemp(temp, proc, 1),
                n.f2.accept(this),
                context.getTemp(tempexp, proc, 0)
                ));
        return "";
    }

    /**
     * f0 -> "HLOAD"
     * f1 -> Temp()
     * f2 -> Temp()
     * f3 -> IntegerLiteral()
     */
    public String visit(HLoadStmt n) {
        int temp = getTemp(n.f1);
        int tempexp = getTemp(n.f2);
        String exp = String.format("%s %s", context.getTemp(tempexp, proc, 0), n.f3.accept(this));
        context.saveTemp(proc, temp, n.f0.toString(), exp, true);
        return "";
    }

    /**
     * f0 -> "MOVE"
     * f1 -> Temp()
     * f2 -> Exp()
     */
    public String visit(MoveStmt n) {
        int temp = getTemp(n.f1);
        String exp = n.f2.accept(this);
        context.saveTemp(proc, temp, n.f0.toString(), exp, true);
        return "";
    }

    /**
     * f0 -> "PRINT"
     * f1 -> SimpleExp()
     */
    public String visit(PrintStmt n) {
        String exp = n.f1.accept(this);
        return String.format("%s %s", n.f0.toString(), exp);
    }

    /**
     * f0 -> Call()
     *       | HAllocate()
     *       | BinOp()
     *       | SimpleExp()
     */
    public String visit(Exp n) {
        return n.f0.accept(this);
    }

    /**
     * f0 -> "CALL"
     * f1 -> SimpleExp()
     * f2 -> "("
     * f3 -> ( Temp() )*
     * f4 -> ")"
     */
    public String visit(Call n) {

        if (n.f3.present()) {
            int argnum = 0;
            for ( Enumeration<Node> e = n.f3.elements(); e.hasMoreElements(); ) {
                Temp tmp = (Temp)e.nextElement();
                String exp = context.getTemp(getTemp(tmp), proc, 0);

                if (argnum < KangaSpecification.arguReg.length) {
                    proc.addStmt(String.format("MOVE %s %s", KangaSpecification.arguReg[argnum], exp));
                } else {
                    int exargn = argnum - KangaSpecification.arguReg.length + 1;
                    proc.addStmt(String.format("PASSARG %d %s", exargn, exp));
                }
                argnum++;
            }
            if (argnum > maxcall) {
                maxcall = argnum;
            }
        }

        String exp = n.f1.accept(this);
        context.callFunc(proc);
        proc.addStmt(String.format("%s %s", n.f0.toString(), exp));
        context.callFuncFin(proc);
        return KangaSpecification.returnReg;
    }

    /**
     * f0 -> "HALLOCATE"
     * f1 -> SimpleExp()
     */
    public String visit(HAllocate n) {
        return String.format("%s %s", n.f0.toString(), n.f1.accept(this));
    }

    /**
     * f0 -> Operator()
     * f1 -> Temp()
     * f2 -> SimpleExp()
     */
    public String visit(BinOp n) {
        String temp = context.getTemp(getTemp(n.f1), proc, 1);
        return String.format("%s %s %s", n.f0.accept(this),
                temp,
                n.f2.accept(this)
        );
    }

    /**
     * f0 -> "LT"
     *       | "PLUS"
     *       | "MINUS"
     *       | "TIMES"
     */
    public String visit(Operator n) {
        return n.f0.choice.toString();
    }

    /**
     * f0 -> Temp()
     *       | IntegerLiteral()
     *       | Label()
     */
    public String visit(SimpleExp n) {
        if (n.f0.choice instanceof Temp) {
            return context.getTemp(getTemp((Temp)n.f0.choice), proc, 0);
        }
        return n.f0.accept(this);
    }

    /**
     * f0 -> "TEMP"
     * f1 -> IntegerLiteral()
     */
    public String visit(Temp n) {
        return n.f1.accept(this);
    }

    /**
     * f0 -> <INTEGER_LITERAL>
     */
    public String visit(IntegerLiteral n) {
        return n.f0.toString();
    }

    /**
     * f0 -> <IDENTIFIER>
     */
    public String visit(Label n) {
        return n.f0.toString();
    }
}

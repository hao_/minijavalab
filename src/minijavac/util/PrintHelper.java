package minijavac.util;

import minijavac.Specification;
import minijavac.piglet.exp.Exp;
import minijavac.piglet.exp.Stmt;

import java.io.PrintStream;

public class PrintHelper {
    public static void PigletIndentPrint(PrintStream out, int depth, Exp exp) {
        if (exp instanceof Stmt && ((Stmt) exp).GetLabel() != null) {
            PigletIndentPrintWithLabel(out, depth, ((Stmt) exp).GetLabel());
            return;
        }
        for (int i = 0; i < depth; i++) {
            out.print(Specification.PigletIndent);
        }
    }

    public static void PigletIndentPrintWithLabel(PrintStream out, int depth, String label) {
        out.printf("%1$-7s", label);
        for (int i = 1; i < depth; i++) {
            out.print(Specification.PigletIndent);
        }
    }

    public static void SpigletIndentPrint(PrintStream out, String label) {
        if (label != null) {
            out.printf("%1$-7s", label);
        } else {
            out.print(Specification.PigletIndent);
        }
    }
}

package minijavac.util;

import java.io.PrintStream;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

public class StmtConstraint {
    public Set<Integer> use;
    Set<Integer> kill;
    public Set<Integer> live;
    Vector<String> nextLabel;
    boolean through;

    public StmtConstraint() {
        use = new HashSet<>();
        kill = new HashSet<>();
        live = new HashSet<>();
        nextLabel = new Vector<>();
        through = true;
    }

    public void setNoThrough() {
        through = false;
    }
    public void addUse(Integer i) {
        use.add(i);
    }
    public void addKill(Integer i) {
        kill.add(i);
    }
    public void addNext(String c) {
        nextLabel.addElement(c);
    }
    public void merge(StmtConstraint c) {
        use.addAll(c.use);
        assert(c.kill.size() == 0);
        assert(c.nextLabel.size() == 0);
    }

    void print(PrintStream out) {
        out.printf("%s - %s + Next(%s)", use, kill, nextLabel);
    }
}
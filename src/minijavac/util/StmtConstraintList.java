package minijavac.util;

import java.io.PrintStream;
import java.util.*;



public class StmtConstraintList {
    public Vector<StmtConstraint> consList;
    Map<String, StmtConstraint> consMap;

    public StmtConstraintList() {
        consList = new Vector<>();
        consMap = new HashMap<>();
    }

    public void addCon(StmtConstraint c) {
        consList.addElement(c);
    }
    public void addConWithLabel(StmtConstraint c, String label) {
        consMap.put(label, c);
        addCon(c);
    }

    public void print(PrintStream out) {
        for (StmtConstraint con : consList) {
            con.print(out);
            out.println();
        }
    }

    public void livenessAnalyse() {
        int n = consList.size();

        StmtConstraint con;

        while (true) {
            boolean update = false;
            for (int i = n - 1; i >= 0; i--) {
                Set<Integer> newLive = new HashSet<>();
                con = consList.get(i);
                for (String label: con.nextLabel) {
                    newLive.addAll(consMap.get(label).live);
                }
                if (con.through) {
                    newLive.addAll(consList.get(i+1).live);
                }
                newLive.removeAll(con.kill);
                newLive.addAll(con.use);
                if (!newLive.equals(con.live)) {
                    update = true;
                    con.live = newLive;
                }
            }
            if (!update) {
                break;
            }
        }
    }

    public void printLive(PrintStream out) {
        for (StmtConstraint con: consList) {
            out.println(con.live);
        }
    }

    public Map<Integer, Integer> colorTemp() {
        Map<Integer, Integer> res = new HashMap<>();
        Map<Integer, Integer> number = new HashMap<>();

        int n = 0;
        for (StmtConstraint c: consList) {
            for (Integer i: c.live) {
                if (!number.containsKey(i)) {
                    number.put(i, n++);
                }
            }
        }

        boolean[][] neigh = new boolean[n][n];
        for (StmtConstraint c: consList) {
            for (Integer i: c.live) {
                for (Integer j: c.live)
                    neigh[number.get(i)][number.get(j)] = true;
            }
        }

        int colorn = 0;
        for (Map.Entry<Integer, Integer> i: number.entrySet()) {
            boolean[] used = new boolean[colorn];
            int realki = i.getKey();
            int ki = number.get(realki);
            for (Map.Entry<Integer, Integer> j: res.entrySet()) {
                int kj = number.get(j.getKey());
                int vj = j.getValue();
                if (ki != kj && neigh[ki][kj] && !used[vj]) {
                    used[vj] = true;
                }
            }
            for (int j = 0; j  < colorn; j++) {
                if (!used[j]) {
                    res.put(realki, j);
                }
            }
            if (!res.containsKey(realki)) {
                res.put(realki, colorn++);
            }
        }

        return res;
    }
}